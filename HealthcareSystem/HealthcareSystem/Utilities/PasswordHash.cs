﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSystem.Utilities
{
    /// <summary>
    /// Utility to hash the password.
    /// </summary>
    class PasswordHash
    {

        /// <summary>
        /// Hashes the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns> the hashed password</returns>
        public static string HashPassword(string password)
        {
            return $"{password.GetHashCode():X}";
        }
    }
}
