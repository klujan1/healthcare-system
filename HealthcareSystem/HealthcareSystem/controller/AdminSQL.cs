﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.DAL.Repository;
using HealthCareSystem.DAL.Repository;

namespace HealthcareSystem.controller
{
    public class AdminSQL
    {
        private SQLTerminalRepository _terminalRepository;

        public AdminSQL()
        {
            this._terminalRepository = new SQLTerminalRepository();
        }
        public void SubmitSQL(string sql)
        {
            this._terminalRepository.SubmitSQL(sql);
        }

        public List<string> getColumnNames()
        {
            return this._terminalRepository.Columns;
        }

        public List<string> getRows()
        {
            return this._terminalRepository.Rows;
        }

        public int getNumberOfColumns()
        {
            return this._terminalRepository.NumberOfColumns;
        }
    }
}
