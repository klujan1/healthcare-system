﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.model;

namespace HealthcareSystem.controller
{
    class AppointmentController
    {

        public void AddAppointment(Appointment appointment)
        {
            AppointmentRepository repo = new AppointmentRepository();
            repo.Add(appointment);
        }

        public Appointment GetAppointmentById(int appointmentID)
        {
            AppointmentRepository repo = new AppointmentRepository();
            return repo.GetById(appointmentID);
        }

        public IList<Appointment> GetAllAppointmentsByPatientId(int patientid)
        {
            return new AppointmentRepository().GetAllByPatientId(patientid);
        }
    }
}
