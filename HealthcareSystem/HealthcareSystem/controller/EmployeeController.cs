﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthCareSystem.DAL.Repository;
using HealthCareSystem.model;

namespace HealthcareSystem.controller
{
    class EmployeeController
    {

        public IList<Employee> GetAllDoctors()
        {
            return new EmployeeRepository().GetAllDoctors();
        }
    }
}
