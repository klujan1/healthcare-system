﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthCareSystem.DAL.Repository;
using HealthCareSystem.model;

namespace HealthcareSystem.controller
{
    public static class AccountRetrieval
    {
        public static Employee GetEmployeeInfo(int id) => new EmployeeRepository().GetById(id);

        public static Patient GetPatientInfo(int id) => new PatientRepository().GetById(id);
    }
}
