﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthCareSystem.DAL.Repository;
using HealthCareSystem.model;

namespace HealthCareSystem.controller
{
    /*
     * Allows the addition of a patient to the database.
     */
    public class PatientController

    {

        /*
         * Adds a patient to the database.
         * 
         * @param patient the patient to be added
         */
        public void Add(Patient patient)
        {
            PatientRepository repo = new PatientRepository();

            repo.Add(patient);
        }

        public void Delete(Patient patient)
        {
            PatientRepository repo = new PatientRepository();
            repo.Delete(patient);
        }

        public IList<Patient> GetAllPatients()
        {
            return new PatientRepository().GetAll();
        }

    }
}
