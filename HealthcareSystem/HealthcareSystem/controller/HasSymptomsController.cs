﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.model;

namespace HealthcareSystem.controller
{
    class HasSymptomsController
    {
        public void AddToHasSymptom(HasSymptom symptom)
        {
            new HasSymptomsRepository().Add(symptom);
        }

        public IList<Symptom> GetAllSymptomsByID(int appointmentID)
        {
            return new HasSymptomsRepository().GetAllSymptomsByID(appointmentID);
        }
    }
}
