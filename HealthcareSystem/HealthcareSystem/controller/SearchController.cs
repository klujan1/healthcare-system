﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.model;
using HealthCareSystem.DAL.Repository;
using HealthCareSystem.model;

namespace HealthCareSystem.controller
{
    public class SearchController
    {
        /*
         * Searches by the desired name, whether it is first name, last name, or full name. 
         * 
         * @param firstName the first name of the patient
         * @param middleInitial the middle initial of the patient
         * @parm lastName the last name of the patient
         * 
         * @return the list of patients under that name or null if none are found
         */
        public IList<Patient> SearchByName(string firstName, string lastName)
        {
            if (firstName == ""  && lastName == "")
            {
                return null;
            }
            else if (firstName != ""  && lastName == "Last Name")
            {
                return new PatientRepository().GetByFirstName(firstName);
            }

            else if (firstName != ""  && lastName != "")
            {
                return new PatientRepository().GetByFirstAndLastName(firstName, lastName);
            }

            else if (firstName == "First Name" && lastName != "")
            {
                return new PatientRepository().GetByLastName(lastName);
            }

            else
            {
                return null;
            }

        }

        /*
         * Gets all the patients with the specified birth date.
         * 
         * @param birthDate the birthdate of the patient
         * 
         * @return the list of patients that have the specified birth date or null if none are found
         */
        public IList<Patient> SearchByBirthDate(DateTime birthDate)
        {
            return new PatientRepository().GetByBirthDate(birthDate);
            
        }

        public IList<Patient> SearchByBirthDateAndName(DateTime birthDate, string firstName = "", string lastName = "")
        {
            return new PatientRepository().GetByBirthDateAndName(birthDate, firstName, lastName);
        }

    }
}
