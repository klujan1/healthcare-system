﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.model;

namespace HealthcareSystem.controller
{
    class RoutineChecksController
    {

        /// <summary>
        /// Adds the specified checks.
        /// </summary>
        /// <param name="checks">The checks.</param>
        public void Add(RoutineCheck checks)
        {
            RoutineChecksRepository repo = new RoutineChecksRepository();
            repo.Add(checks);
        }

        /// <summary>
        /// Gets the by appointment identifier.
        /// </summary>
        /// <param name="appointmentId">The appointment identifier.</param>
        /// <returns> the routine checks for that appointment</returns>
        public RoutineCheck GetByAppointmentId(int appointmentId)
        {
            return new RoutineChecksRepository().GetById(appointmentId);
        }
    }
}
