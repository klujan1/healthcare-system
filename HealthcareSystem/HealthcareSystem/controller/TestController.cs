﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.model;

namespace HealthcareSystem.controller
{
    class TestController
    {
        public IList<Test> GetAllTests()
        {
            return new TestRepository().GetAll();
        }

        public Test GetAllTests(string name)
        {
            return new TestRepository().GetByName(name);
        }

        public void AddTest(Test test)
        {
            new TestRepository().Add(test);
        }
    }
}
