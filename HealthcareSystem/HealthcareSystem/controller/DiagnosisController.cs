﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.model;

namespace HealthcareSystem.controller
{
    class DiagnosisController
    {
        public void Add(Diagnosis diagnosis)
        {
            new DiagnosisRepository().Add(diagnosis);
        }

        public Diagnosis GetAllDiagnosisesByAppointmentID(int appointmentID)
        {
            return new DiagnosisRepository().GetAllDiagnosisByAppointmentID(appointmentID);
        }

        public Diagnosis GetDiagnosisByID(int id)
        {
            return new DiagnosisRepository().GetById(id);
        }

        public void FinalizeDiagnosis(Diagnosis diagnosis)
        {
            new DiagnosisRepository().FinalizeDiagnosis(diagnosis);
        }

        public bool isFinalDiagnosisNull(int id)
        {
            return new DiagnosisRepository().isFinalDiagnosisNull(id);
        }
        public bool isInitialDiagnosisNull(int id)
        {
            return new DiagnosisRepository().isInitialDiagnosisNull(id);
        }
    }
}
