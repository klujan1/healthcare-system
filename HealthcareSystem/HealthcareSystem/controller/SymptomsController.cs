﻿using System.Collections;
using System.Collections.Generic;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.model;

namespace HealthcareSystem.controller
{
    public class SymptomsController
    {
        public IList<Symptom> GetAllSymptoms()
        {
            return new SymptomsRepository().GetAll();
        }
    }
}