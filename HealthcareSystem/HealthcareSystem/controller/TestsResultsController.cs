﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.model;

namespace HealthcareSystem.controller
{
    class TestsResultsController
    {
        public IList<TestResult> GetTestResultsByPatientID(int patientID)
        {
            return new TestResultsRepository().GetAllByPatientId(patientID);
        }
    }
}
