﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.model;

namespace HealthcareSystem.controller
{
    class TestsOrderedController
    {
        public IList<TestOrdered> GetAllOrderedTestsByID(int diagnosisID)
        {
            return new TestsOrderedRepository().GetAllOrderedTestsByID(diagnosisID);
        }

        public void AddOrderTest(TestOrdered tesetOrder)
        {
            new TestsOrderedRepository().Add(tesetOrder);
        }
    }
}
