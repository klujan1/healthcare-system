﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.Utilities;
using HealthCareSystem.model;
using HealthCareSystem.DAL.Repository;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.controller
{
    /*
     * Handles account verification.
     */
    public static class AccountVerificationController
    {
        /*
         * Checks if the Id and password are valid.
         * 
         * @param Id the Id of the employee
         * @param password the employee's password
         * 
         * @return if the account is valid 
         */
        public static bool CheckIDAndPassword(int idNumber, string password)
        {
            var account = new PasswordsRepository().GetById(idNumber);
            password = PasswordHash.HashPassword(password);
            try
            {
                bool test = account.Password == password;
                return account.Id == idNumber && account.Password == password;
            }
            catch (NullReferenceException exception)
            {
                return false;
            }
            return false;
        }

        public static bool isAdmin(int idNumber)
        {
            return new EmployeeRepository().isAdmin(idNumber);
        }

        public static bool isDoctor(int idNumber)
        {
            return new EmployeeRepository().isDoctor(idNumber);
        }

    }


}
