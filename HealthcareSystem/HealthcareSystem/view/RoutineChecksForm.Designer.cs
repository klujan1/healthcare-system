﻿namespace HealthcareSystem.view
{
    partial class RoutineChecksForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoutineChecksForm));
            this.bloodPressureLabel = new System.Windows.Forms.Label();
            this.bodyTemperatureLabel = new System.Windows.Forms.Label();
            this.pulseLabel = new System.Windows.Forms.Label();
            this.ststolicReadingLabel = new System.Windows.Forms.Label();
            this.diastolicReadingLabel = new System.Windows.Forms.Label();
            this.routineChecksSubmitButton = new System.Windows.Forms.Button();
            this.numeratorBloodPressureTextBox = new System.Windows.Forms.TextBox();
            this.denominatorBloodPressureTextBox = new System.Windows.Forms.TextBox();
            this.backSlashBloodPressureLabel = new System.Windows.Forms.Label();
            this.bodyTemperatureTextbox = new System.Windows.Forms.TextBox();
            this.pulseTextBox = new System.Windows.Forms.TextBox();
            this.systolicReadingTextBox = new System.Windows.Forms.TextBox();
            this.diastolicReadingTextBox = new System.Windows.Forms.TextBox();
            this.error1 = new System.Windows.Forms.Label();
            this.error3 = new System.Windows.Forms.Label();
            this.error2 = new System.Windows.Forms.Label();
            this.symptomsDataGridView = new System.Windows.Forms.DataGridView();
            this.symptomCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.patientSymptoms = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patientSymptomsLabel = new System.Windows.Forms.Label();
            this.noSymptomsCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.symptomsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // bloodPressureLabel
            // 
            this.bloodPressureLabel.AutoSize = true;
            this.bloodPressureLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bloodPressureLabel.Location = new System.Drawing.Point(60, 44);
            this.bloodPressureLabel.Name = "bloodPressureLabel";
            this.bloodPressureLabel.Size = new System.Drawing.Size(87, 15);
            this.bloodPressureLabel.TabIndex = 0;
            this.bloodPressureLabel.Text = "Blood Pressure";
            // 
            // bodyTemperatureLabel
            // 
            this.bodyTemperatureLabel.AutoSize = true;
            this.bodyTemperatureLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bodyTemperatureLabel.Location = new System.Drawing.Point(43, 90);
            this.bodyTemperatureLabel.Name = "bodyTemperatureLabel";
            this.bodyTemperatureLabel.Size = new System.Drawing.Size(104, 15);
            this.bodyTemperatureLabel.TabIndex = 1;
            this.bodyTemperatureLabel.Text = "Body Temperature";
            // 
            // pulseLabel
            // 
            this.pulseLabel.AutoSize = true;
            this.pulseLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pulseLabel.Location = new System.Drawing.Point(111, 136);
            this.pulseLabel.Name = "pulseLabel";
            this.pulseLabel.Size = new System.Drawing.Size(36, 15);
            this.pulseLabel.TabIndex = 2;
            this.pulseLabel.Text = "Pulse";
            // 
            // ststolicReadingLabel
            // 
            this.ststolicReadingLabel.AutoSize = true;
            this.ststolicReadingLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ststolicReadingLabel.Location = new System.Drawing.Point(52, 182);
            this.ststolicReadingLabel.Name = "ststolicReadingLabel";
            this.ststolicReadingLabel.Size = new System.Drawing.Size(95, 15);
            this.ststolicReadingLabel.TabIndex = 3;
            this.ststolicReadingLabel.Text = "Systolic Reading";
            // 
            // diastolicReadingLabel
            // 
            this.diastolicReadingLabel.AutoSize = true;
            this.diastolicReadingLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diastolicReadingLabel.Location = new System.Drawing.Point(46, 228);
            this.diastolicReadingLabel.Name = "diastolicReadingLabel";
            this.diastolicReadingLabel.Size = new System.Drawing.Size(101, 15);
            this.diastolicReadingLabel.TabIndex = 4;
            this.diastolicReadingLabel.Text = "Diastolic Reading";
            // 
            // routineChecksSubmitButton
            // 
            this.routineChecksSubmitButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.routineChecksSubmitButton.Location = new System.Drawing.Point(270, 285);
            this.routineChecksSubmitButton.Name = "routineChecksSubmitButton";
            this.routineChecksSubmitButton.Size = new System.Drawing.Size(124, 32);
            this.routineChecksSubmitButton.TabIndex = 5;
            this.routineChecksSubmitButton.Text = "Submit";
            this.routineChecksSubmitButton.UseVisualStyleBackColor = true;
            this.routineChecksSubmitButton.Click += new System.EventHandler(this.routineChecksSubmitButton_Click);
            // 
            // numeratorBloodPressureTextBox
            // 
            this.numeratorBloodPressureTextBox.Location = new System.Drawing.Point(179, 43);
            this.numeratorBloodPressureTextBox.Name = "numeratorBloodPressureTextBox";
            this.numeratorBloodPressureTextBox.Size = new System.Drawing.Size(37, 20);
            this.numeratorBloodPressureTextBox.TabIndex = 1;
            this.numeratorBloodPressureTextBox.TextChanged += new System.EventHandler(this.numeratorBloodPressureTextBox_TextChanged);
            // 
            // denominatorBloodPressureTextBox
            // 
            this.denominatorBloodPressureTextBox.Location = new System.Drawing.Point(242, 43);
            this.denominatorBloodPressureTextBox.Name = "denominatorBloodPressureTextBox";
            this.denominatorBloodPressureTextBox.Size = new System.Drawing.Size(37, 20);
            this.denominatorBloodPressureTextBox.TabIndex = 2;
            this.denominatorBloodPressureTextBox.TextChanged += new System.EventHandler(this.denominatorBloodPressureTextBox_TextChanged);
            // 
            // backSlashBloodPressureLabel
            // 
            this.backSlashBloodPressureLabel.AutoSize = true;
            this.backSlashBloodPressureLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backSlashBloodPressureLabel.Location = new System.Drawing.Point(224, 45);
            this.backSlashBloodPressureLabel.Name = "backSlashBloodPressureLabel";
            this.backSlashBloodPressureLabel.Size = new System.Drawing.Size(13, 18);
            this.backSlashBloodPressureLabel.TabIndex = 8;
            this.backSlashBloodPressureLabel.Text = "/";
            // 
            // bodyTemperatureTextbox
            // 
            this.bodyTemperatureTextbox.Location = new System.Drawing.Point(179, 88);
            this.bodyTemperatureTextbox.Name = "bodyTemperatureTextbox";
            this.bodyTemperatureTextbox.Size = new System.Drawing.Size(100, 20);
            this.bodyTemperatureTextbox.TabIndex = 3;
            // 
            // pulseTextBox
            // 
            this.pulseTextBox.Location = new System.Drawing.Point(179, 134);
            this.pulseTextBox.Name = "pulseTextBox";
            this.pulseTextBox.Size = new System.Drawing.Size(100, 20);
            this.pulseTextBox.TabIndex = 4;
            // 
            // systolicReadingTextBox
            // 
            this.systolicReadingTextBox.Enabled = false;
            this.systolicReadingTextBox.Location = new System.Drawing.Point(179, 180);
            this.systolicReadingTextBox.Name = "systolicReadingTextBox";
            this.systolicReadingTextBox.Size = new System.Drawing.Size(100, 20);
            this.systolicReadingTextBox.TabIndex = 11;
            // 
            // diastolicReadingTextBox
            // 
            this.diastolicReadingTextBox.Enabled = false;
            this.diastolicReadingTextBox.Location = new System.Drawing.Point(179, 226);
            this.diastolicReadingTextBox.Name = "diastolicReadingTextBox";
            this.diastolicReadingTextBox.Size = new System.Drawing.Size(100, 20);
            this.diastolicReadingTextBox.TabIndex = 12;
            // 
            // error1
            // 
            this.error1.AutoSize = true;
            this.error1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error1.ForeColor = System.Drawing.Color.Red;
            this.error1.Location = new System.Drawing.Point(286, 47);
            this.error1.Name = "error1";
            this.error1.Size = new System.Drawing.Size(77, 16);
            this.error1.TabIndex = 20;
            this.error1.Text = "Invalid input";
            this.error1.Visible = false;
            // 
            // error3
            // 
            this.error3.AutoSize = true;
            this.error3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error3.ForeColor = System.Drawing.Color.Red;
            this.error3.Location = new System.Drawing.Point(286, 137);
            this.error3.Name = "error3";
            this.error3.Size = new System.Drawing.Size(77, 16);
            this.error3.TabIndex = 21;
            this.error3.Text = "Invalid input";
            this.error3.Visible = false;
            // 
            // error2
            // 
            this.error2.AutoSize = true;
            this.error2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error2.ForeColor = System.Drawing.Color.Red;
            this.error2.Location = new System.Drawing.Point(286, 92);
            this.error2.Name = "error2";
            this.error2.Size = new System.Drawing.Size(77, 16);
            this.error2.TabIndex = 24;
            this.error2.Text = "Invalid input";
            this.error2.Visible = false;
            // 
            // symptomsDataGridView
            // 
            this.symptomsDataGridView.AllowUserToAddRows = false;
            this.symptomsDataGridView.AllowUserToDeleteRows = false;
            this.symptomsDataGridView.AllowUserToResizeColumns = false;
            this.symptomsDataGridView.AllowUserToResizeRows = false;
            this.symptomsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.symptomsDataGridView.ColumnHeadersVisible = false;
            this.symptomsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.symptomCheckBox,
            this.patientSymptoms});
            this.symptomsDataGridView.Location = new System.Drawing.Point(383, 70);
            this.symptomsDataGridView.Name = "symptomsDataGridView";
            this.symptomsDataGridView.ReadOnly = true;
            this.symptomsDataGridView.RowHeadersVisible = false;
            this.symptomsDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.symptomsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.symptomsDataGridView.Size = new System.Drawing.Size(233, 173);
            this.symptomsDataGridView.TabIndex = 25;
            this.symptomsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.symptomsDataGridView_CellContentClick);
            // 
            // symptomCheckBox
            // 
            this.symptomCheckBox.HeaderText = "checkbox";
            this.symptomCheckBox.Name = "symptomCheckBox";
            this.symptomCheckBox.ReadOnly = true;
            this.symptomCheckBox.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.symptomCheckBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.symptomCheckBox.Width = 30;
            // 
            // patientSymptoms
            // 
            this.patientSymptoms.HeaderText = "Symptoms";
            this.patientSymptoms.Name = "patientSymptoms";
            this.patientSymptoms.ReadOnly = true;
            this.patientSymptoms.Width = 200;
            // 
            // patientSymptomsLabel
            // 
            this.patientSymptomsLabel.AutoSize = true;
            this.patientSymptomsLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientSymptomsLabel.Location = new System.Drawing.Point(380, 47);
            this.patientSymptomsLabel.Name = "patientSymptomsLabel";
            this.patientSymptomsLabel.Size = new System.Drawing.Size(59, 15);
            this.patientSymptomsLabel.TabIndex = 26;
            this.patientSymptomsLabel.Text = "Symptoms";
            // 
            // noSymptomsCheckBox
            // 
            this.noSymptomsCheckBox.AutoSize = true;
            this.noSymptomsCheckBox.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noSymptomsCheckBox.Location = new System.Drawing.Point(456, 260);
            this.noSymptomsCheckBox.Name = "noSymptomsCheckBox";
            this.noSymptomsCheckBox.Size = new System.Drawing.Size(100, 20);
            this.noSymptomsCheckBox.TabIndex = 27;
            this.noSymptomsCheckBox.Text = "No Symptoms";
            this.noSymptomsCheckBox.UseVisualStyleBackColor = true;
            this.noSymptomsCheckBox.CheckedChanged += new System.EventHandler(this.noSymptomsCheckBox_CheckedChanged);
            // 
            // RoutineChecksForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 353);
            this.Controls.Add(this.noSymptomsCheckBox);
            this.Controls.Add(this.patientSymptomsLabel);
            this.Controls.Add(this.symptomsDataGridView);
            this.Controls.Add(this.error2);
            this.Controls.Add(this.error3);
            this.Controls.Add(this.error1);
            this.Controls.Add(this.diastolicReadingTextBox);
            this.Controls.Add(this.systolicReadingTextBox);
            this.Controls.Add(this.pulseTextBox);
            this.Controls.Add(this.bodyTemperatureTextbox);
            this.Controls.Add(this.backSlashBloodPressureLabel);
            this.Controls.Add(this.denominatorBloodPressureTextBox);
            this.Controls.Add(this.numeratorBloodPressureTextBox);
            this.Controls.Add(this.routineChecksSubmitButton);
            this.Controls.Add(this.diastolicReadingLabel);
            this.Controls.Add(this.ststolicReadingLabel);
            this.Controls.Add(this.pulseLabel);
            this.Controls.Add(this.bodyTemperatureLabel);
            this.Controls.Add(this.bloodPressureLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RoutineChecksForm";
            this.Text = "Routine Checks";
            ((System.ComponentModel.ISupportInitialize)(this.symptomsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label bloodPressureLabel;
        private System.Windows.Forms.Label bodyTemperatureLabel;
        private System.Windows.Forms.Label pulseLabel;
        private System.Windows.Forms.Label ststolicReadingLabel;
        private System.Windows.Forms.Label diastolicReadingLabel;
        private System.Windows.Forms.Button routineChecksSubmitButton;
        private System.Windows.Forms.TextBox numeratorBloodPressureTextBox;
        private System.Windows.Forms.TextBox denominatorBloodPressureTextBox;
        private System.Windows.Forms.Label backSlashBloodPressureLabel;
        private System.Windows.Forms.TextBox bodyTemperatureTextbox;
        private System.Windows.Forms.TextBox pulseTextBox;
        private System.Windows.Forms.TextBox systolicReadingTextBox;
        private System.Windows.Forms.TextBox diastolicReadingTextBox;
        private System.Windows.Forms.Label error1;
        private System.Windows.Forms.Label error3;
        private System.Windows.Forms.Label error2;
        private System.Windows.Forms.DataGridView symptomsDataGridView;
        private System.Windows.Forms.Label patientSymptomsLabel;
        private System.Windows.Forms.DataGridViewCheckBoxColumn symptomCheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn patientSymptoms;
        private System.Windows.Forms.CheckBox noSymptomsCheckBox;
    }
}