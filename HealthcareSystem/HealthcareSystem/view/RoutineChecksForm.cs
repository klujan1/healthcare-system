﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using HealthcareSystem.controller;
using HealthcareSystem.model;

namespace HealthcareSystem.view
{
    public partial class RoutineChecksForm : Form
    {
        private int currentPatientID;
        private int currentNurseID;
        private int currentAppointmentID;
        private bool pulseIsCorrect;
        private bool bloodPressureIsCorrect;
        private bool bodyTempIsCorrect;

        private PatientAppointmentForm appointmentForm;
        public RoutineChecksForm(int patientID, int nurseID, int appointmentID, PatientAppointmentForm form)
        {
            InitializeComponent();
            this.HideErrorMessages();

            this.currentPatientID = patientID;
            this.currentNurseID = nurseID;
            this.currentAppointmentID = appointmentID;

            this.appointmentForm = form;
            this.appointmentForm.Hide();
            this.PopulateSymptomsTable();
        }

        private void HideErrorMessages()
        {
            this.error1.Hide();
            this.error2.Hide();
            this.error3.Hide();
        }

        private bool IsDigitsOnly(string input)
        {
            input.Trim();

            if (input.Length == 0)
            {
                return false;
            }

            foreach (char letter in input)
            {
                if (letter < '0' || letter > '9')
                    return false;
            }

            return true;
        }

        private void numeratorBloodPressureTextBox_TextChanged(object sender, EventArgs e)
        {
           
            this.systolicReadingTextBox.Text = this.numeratorBloodPressureTextBox.Text;
        }

        private void denominatorBloodPressureTextBox_TextChanged(object sender, EventArgs e)
        {
     
            this.diastolicReadingTextBox.Text = this.denominatorBloodPressureTextBox.Text;
        }
       
        private void CheckAllEnteredFields()
        {
             this.pulseIsCorrect = this.CheckPulseFormat();
             this.bloodPressureIsCorrect = this.CheckBloodPressureFormat();
             this.bodyTempIsCorrect = this.CheckBodyTemperatureFormat();
        }

        private bool CheckBloodPressureFormat()
        {
            bool correctFormat;

            if (!this.IsDigitsOnly(numeratorBloodPressureTextBox.Text))
            {
                this.error1.Show();
                correctFormat = false;
            }
            else
            {
                this.error1.Hide();
                correctFormat = true;
            }

            return correctFormat;
        }

        private bool CheckBodyTemperatureFormat()
        {
            bool correctFormat;

            if (!this.IsDigitsOnly(this.bodyTemperatureTextbox.Text))
            {
                this.error2.Show();
                correctFormat = false;
            }
            else
            {
                this.error2.Hide();
                correctFormat = true;
            }

            return correctFormat;
        }

        private bool CheckPulseFormat()
        {
            bool correctFormat;
            if (!this.IsDigitsOnly(this.pulseTextBox.Text))
            {
                this.error3.Show();
                correctFormat = false;
            }
            else
            {
                this.error3.Hide();
                correctFormat = true;
            }
            return correctFormat;
        }

        private void routineChecksSubmitButton_Click(object sender, EventArgs e)
        {
            this.CheckAllEnteredFields();

            if (this.allFieldAreCorrect() && this.atLeastOneSymptomIsChecked())
            {
                RoutineChecksController routineChecksController = new RoutineChecksController();
                RoutineCheck routineCheck = new RoutineCheck();

                routineCheck.NurseID = this.currentNurseID;
                routineCheck.AppointmentID = this.currentAppointmentID;
                routineCheck.BodyTemp = Int32.Parse(this.bodyTemperatureTextbox.Text);
                routineCheck.PulseBPM = Int32.Parse(this.pulseTextBox.Text);
                routineCheck.SystolicReading = Int32.Parse(this.systolicReadingTextBox.Text);
                routineCheck.DiastolicReading = Int32.Parse(this.diastolicReadingTextBox.Text);
                routineCheck.SetBloodPressure();

                routineChecksController.Add(routineCheck);
                this.addAllSelectedSymptoms();
                MessageBox.Show("The information has been updated");
                this.appointmentForm.Show();
                this.appointmentForm.RefreshRoutineCheckInformation();
                this.Close();
               

            } 
            else if (!this.allFieldAreCorrect())
            {
                MessageBox.Show("One of the fields has been entered incorrectly or is blank.");
            }
            else if (!this.atLeastOneSymptomIsChecked())
            {
                MessageBox.Show("You must select at least one symptom. If there are no symptoms, check no sysmtoms");
            }
        }

        private void addAllSelectedSymptoms()
        {
            HasSymptomsController controller = new HasSymptomsController();

            foreach (var symptom in this.getSelectedSymptoms())
            {
                HasSymptom hasSymptom = new HasSymptom();
                hasSymptom.AppointmentID = this.currentAppointmentID;
                hasSymptom.SymptomName = symptom.Name;
                controller.AddToHasSymptom(hasSymptom);
            }
        }

        private IList<Symptom> getSelectedSymptoms()
        {
            IList<Symptom> selectedSymptoms = new List<Symptom>();

            foreach(DataGridViewRow symptom in this.symptomsDataGridView.Rows)
            {
                if (Convert.ToBoolean(symptom.Cells[0].Value))
                {
                    Symptom tempSymptom = new Symptom();
                    tempSymptom.Name = symptom.Cells[1].Value.ToString();
                    selectedSymptoms.Add(tempSymptom);
                }
            }

            return selectedSymptoms;

        } 
        private bool atLeastOneSymptomIsChecked()
        {
            bool atLeastOneChecked = false;
            foreach (DataGridViewRow symptom in this.symptomsDataGridView.Rows)
            {
                if (Convert.ToBoolean(symptom.Cells[0].Value.ToString()))
                {
                    atLeastOneChecked = true;
                }
            }

            return atLeastOneChecked;
        }

        private bool allFieldAreCorrect()
        {
            bool allCorrect;

            if (this.bloodPressureIsCorrect && this.bodyTempIsCorrect && this.pulseIsCorrect)
            {
                allCorrect = true;
            }
            else
            {
                allCorrect = false;
            }

            return allCorrect;
        }

        private void PopulateSymptomsTable()
        {
            SymptomsController controller = new SymptomsController();
            IList<Symptom> symptoms = controller.GetAllSymptoms();

            foreach (var symptom in symptoms)
            {
                this.symptomsDataGridView.Rows.Add(false, symptom.Name);
            }
            
        }

        private void symptomsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            bool isChecked = Convert.ToBoolean(this.symptomsDataGridView.SelectedRows[0].Cells[0].Value);

            if (!isChecked)
            {
                this.symptomsDataGridView.SelectedRows[0].Cells[0].Value = true;
            }
            else
            {
                this.symptomsDataGridView.SelectedRows[0].Cells[0].Value = false;

            }

            if (!isChecked && symptomsDataGridView.SelectedRows[0].Cells[1].Value.ToString() == "No symptoms")
            {
                this.symptomsDataGridView.Enabled = false;
                this.noSymptomsCheckBox.Checked = true;
            }
        }

        private void noSymptomsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.noSymptomsCheckBox.Checked)
            {
                this.symptomsDataGridView.Enabled = false;
                this.UncheckAllSymptomsInDataGridView();
                
            }
            else
            {
                this.symptomsDataGridView.Enabled = true;
                this.UncheckNoSymptomInDataGridView();
            }
        }

        private void UncheckNoSymptomInDataGridView()
        {
            foreach (DataGridViewRow symptom in this.symptomsDataGridView.Rows)
            {
                if (symptom.Cells[1].Value.ToString() == "No symptoms")
                {
                    symptom.Cells[0].Value = false;
                }
               
            }
        }

        private void UncheckAllSymptomsInDataGridView()
        {
            foreach (DataGridViewRow symptom in this.symptomsDataGridView.Rows)
            {
                if (symptom.Cells[1].Value.ToString() != "No symptoms")
                {
                    symptom.Cells[0].Value = false;
                }
                else
                {
                    symptom.Cells[0].Value = true;
                }

            }
        }
    }
}
