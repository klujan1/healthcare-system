﻿namespace HealthcareSystem.view
{
    partial class NewAppointmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewAppointmentForm));
            this.patientNameLabel = new System.Windows.Forms.Label();
            this.doctorNameLabel = new System.Windows.Forms.Label();
            this.reasonForVisitLabel = new System.Windows.Forms.Label();
            this.scheduleAppointmentButton = new System.Windows.Forms.Button();
            this.reasonForVisitTextBox = new System.Windows.Forms.TextBox();
            this.doctorComboBox = new System.Windows.Forms.ComboBox();
            this.patientNameFeild = new System.Windows.Forms.Label();
            this.dateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.appointmentDateLabel = new System.Windows.Forms.Label();
            this.timeField = new System.Windows.Forms.Label();
            this.timeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // patientNameLabel
            // 
            this.patientNameLabel.AutoSize = true;
            this.patientNameLabel.Location = new System.Drawing.Point(91, 33);
            this.patientNameLabel.Name = "patientNameLabel";
            this.patientNameLabel.Size = new System.Drawing.Size(40, 13);
            this.patientNameLabel.TabIndex = 0;
            this.patientNameLabel.Text = "Patient";
            // 
            // doctorNameLabel
            // 
            this.doctorNameLabel.AutoSize = true;
            this.doctorNameLabel.Location = new System.Drawing.Point(92, 147);
            this.doctorNameLabel.Name = "doctorNameLabel";
            this.doctorNameLabel.Size = new System.Drawing.Size(39, 13);
            this.doctorNameLabel.TabIndex = 1;
            this.doctorNameLabel.Text = "Doctor";
            // 
            // reasonForVisitLabel
            // 
            this.reasonForVisitLabel.AutoSize = true;
            this.reasonForVisitLabel.Location = new System.Drawing.Point(50, 187);
            this.reasonForVisitLabel.Name = "reasonForVisitLabel";
            this.reasonForVisitLabel.Size = new System.Drawing.Size(81, 13);
            this.reasonForVisitLabel.TabIndex = 2;
            this.reasonForVisitLabel.Text = "Reason for Visit";
            // 
            // scheduleAppointmentButton
            // 
            this.scheduleAppointmentButton.Location = new System.Drawing.Point(157, 252);
            this.scheduleAppointmentButton.Name = "scheduleAppointmentButton";
            this.scheduleAppointmentButton.Size = new System.Drawing.Size(75, 23);
            this.scheduleAppointmentButton.TabIndex = 3;
            this.scheduleAppointmentButton.Text = "Schdule";
            this.scheduleAppointmentButton.UseVisualStyleBackColor = true;
            this.scheduleAppointmentButton.Click += new System.EventHandler(this.scheduleAppointmentButton_Click);
            // 
            // reasonForVisitTextBox
            // 
            this.reasonForVisitTextBox.Location = new System.Drawing.Point(157, 187);
            this.reasonForVisitTextBox.Multiline = true;
            this.reasonForVisitTextBox.Name = "reasonForVisitTextBox";
            this.reasonForVisitTextBox.Size = new System.Drawing.Size(201, 47);
            this.reasonForVisitTextBox.TabIndex = 4;
            // 
            // doctorComboBox
            // 
            this.doctorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.doctorComboBox.FormattingEnabled = true;
            this.doctorComboBox.Location = new System.Drawing.Point(157, 144);
            this.doctorComboBox.Name = "doctorComboBox";
            this.doctorComboBox.Size = new System.Drawing.Size(201, 21);
            this.doctorComboBox.TabIndex = 7;
            // 
            // patientNameFeild
            // 
            this.patientNameFeild.AutoSize = true;
            this.patientNameFeild.Location = new System.Drawing.Point(161, 33);
            this.patientNameFeild.Name = "patientNameFeild";
            this.patientNameFeild.Size = new System.Drawing.Size(71, 13);
            this.patientNameFeild.TabIndex = 8;
            this.patientNameFeild.Text = "Patient Name";
            // 
            // dateDateTimePicker
            // 
            this.dateDateTimePicker.Checked = false;
            this.dateDateTimePicker.Location = new System.Drawing.Point(157, 69);
            this.dateDateTimePicker.MinDate = new System.DateTime(2016, 11, 29, 20, 28, 20, 0);
            this.dateDateTimePicker.Name = "dateDateTimePicker";
            this.dateDateTimePicker.Size = new System.Drawing.Size(201, 20);
            this.dateDateTimePicker.TabIndex = 9;
            this.dateDateTimePicker.Value = new System.DateTime(2016, 11, 29, 20, 28, 20, 0);
            // 
            // appointmentDateLabel
            // 
            this.appointmentDateLabel.AutoSize = true;
            this.appointmentDateLabel.Location = new System.Drawing.Point(101, 76);
            this.appointmentDateLabel.Name = "appointmentDateLabel";
            this.appointmentDateLabel.Size = new System.Drawing.Size(30, 13);
            this.appointmentDateLabel.TabIndex = 10;
            this.appointmentDateLabel.Text = "Date";
            // 
            // timeField
            // 
            this.timeField.AutoSize = true;
            this.timeField.Location = new System.Drawing.Point(101, 111);
            this.timeField.Name = "timeField";
            this.timeField.Size = new System.Drawing.Size(30, 13);
            this.timeField.TabIndex = 11;
            this.timeField.Text = "Time";
            // 
            // timeDateTimePicker
            // 
            this.timeDateTimePicker.Checked = false;
            this.timeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeDateTimePicker.Location = new System.Drawing.Point(157, 105);
            this.timeDateTimePicker.MinDate = new System.DateTime(2016, 11, 29, 0, 0, 0, 0);
            this.timeDateTimePicker.Name = "timeDateTimePicker";
            this.timeDateTimePicker.Size = new System.Drawing.Size(201, 20);
            this.timeDateTimePicker.TabIndex = 12;
            // 
            // NewAppointmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 287);
            this.Controls.Add(this.timeDateTimePicker);
            this.Controls.Add(this.timeField);
            this.Controls.Add(this.appointmentDateLabel);
            this.Controls.Add(this.dateDateTimePicker);
            this.Controls.Add(this.patientNameFeild);
            this.Controls.Add(this.doctorComboBox);
            this.Controls.Add(this.reasonForVisitTextBox);
            this.Controls.Add(this.scheduleAppointmentButton);
            this.Controls.Add(this.reasonForVisitLabel);
            this.Controls.Add(this.doctorNameLabel);
            this.Controls.Add(this.patientNameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewAppointmentForm";
            this.Text = "New Appointment";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label patientNameLabel;
        private System.Windows.Forms.Label doctorNameLabel;
        private System.Windows.Forms.Label reasonForVisitLabel;
        private System.Windows.Forms.Button scheduleAppointmentButton;
        private System.Windows.Forms.TextBox reasonForVisitTextBox;
        private System.Windows.Forms.ComboBox doctorComboBox;
        private System.Windows.Forms.Label patientNameFeild;
        private System.Windows.Forms.DateTimePicker dateDateTimePicker;
        private System.Windows.Forms.Label appointmentDateLabel;
        private System.Windows.Forms.Label timeField;
        private System.Windows.Forms.DateTimePicker timeDateTimePicker;
    }
}