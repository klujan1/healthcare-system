﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.DirectoryServices.ActiveDirectory;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HealthcareSystem.controller;
using HealthcareSystem.model;
using HealthCareSystem.controller;
using HealthCareSystem.model;

namespace HealthcareSystem.view
{
    public partial class NewAppointmentForm : Form
    {
        private IList<Employee> doctors = null;
        private Patient patient;
        private Employee doctor;
 
        public NewAppointmentForm()
        {
            InitializeComponent();
        }

        public NewAppointmentForm(Patient selectedPatient)
        {
            InitializeComponent();
            this.patient = selectedPatient;
            SearchController search = new SearchController();
            this.patientNameFeild.Text = this.patient.FirstName + " " + this.patient.Midinit + ". " + this.patient.LastName;

            EmployeeController controller = new EmployeeController();
            this.doctors = controller.GetAllDoctors();            

            foreach (var doctor in this.doctors)
            {
                var formatedDoctorName = "Dr. " + doctor.FirstName + " " + doctor.Midinit + ". " + doctor.LastName;
                this.doctorComboBox.Items.Add(formatedDoctorName);

            }
        }

        private void scheduleAppointmentButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.doctors.Count; i++)
            {
                if (this.doctorComboBox.SelectedIndex == i)
                {
                    this.doctor = this.doctors[i];
                }
            }

           
            Appointment appointment = new Appointment();
            appointment.DoctorID = this.doctor.Id;
            appointment.Date = this.FormateDateTime();
            appointment.PatientID = this.patient.Id;
            appointment.Reason = this.reasonForVisitTextBox.Text;

            AppointmentController controller = new AppointmentController();
            try
            {
                controller.AddAppointment(appointment);
            }
            catch
            {
                MessageBox.Show("An error occured. Please try scheduling the appointment again.");
                this.Close();
                return;
            }
            MessageBox.Show("Appointment was added");
           this.Close(); 
        }

        private DateTime FormateDateTime()
        {
            var year = this.dateDateTimePicker.Value.Year;
            var month = this.dateDateTimePicker.Value.Month;
            var day = this.dateDateTimePicker.Value.Day;
            var hour = this.timeDateTimePicker.Value.Hour;
            var min = this.timeDateTimePicker.Value.Minute;
            var sec = this.timeDateTimePicker.Value.Second;
            DateTime date = new DateTime(year,month,day,hour,min,sec);
            return date;

        }
    }
}
