﻿namespace HealthcareSystem.view
{
    partial class PatientAppointmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatientAppointmentForm));
            this.appointmentIDLabel = new System.Windows.Forms.Label();
            this.appointmentInfo_paientID = new System.Windows.Forms.Label();
            this.dataLabel = new System.Windows.Forms.Label();
            this.reasonForVisitLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.generalAppointmentInformationGroupBox = new System.Windows.Forms.GroupBox();
            this.doctorNameFeild = new System.Windows.Forms.Label();
            this.reasonForVisitFeild = new System.Windows.Forms.Label();
            this.appointmentDateFeild = new System.Windows.Forms.Label();
            this.patientIDFeild = new System.Windows.Forms.Label();
            this.appointmentIDFeild = new System.Windows.Forms.Label();
            this.diagnosisInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.finalDiagnosisTextBox = new System.Windows.Forms.TextBox();
            this.initialDiagnosisTextBox = new System.Windows.Forms.TextBox();
            this.finalDiagnosisSubmitButton = new System.Windows.Forms.Button();
            this.submitInitialDiagnosisButton = new System.Windows.Forms.Button();
            this.finalDiagnosisLabel = new System.Windows.Forms.Label();
            this.initialDiagnosisLabel = new System.Windows.Forms.Label();
            this.testInformationGroupBox = new System.Windows.Forms.GroupBox();
            this.orderedTestDataGridView = new System.Windows.Forms.DataGridView();
            this.testCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.test = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testResultsDataGridView = new System.Windows.Forms.DataGridView();
            this.testName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datePreformed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testResultsLabel = new System.Windows.Forms.Label();
            this.avalibleTestLabel = new System.Windows.Forms.Label();
            this.orderTestButton = new System.Windows.Forms.Button();
            this.routineChecksGroupBox = new System.Windows.Forms.GroupBox();
            this.patientSymptomsDataGridView = new System.Windows.Forms.DataGridView();
            this.patientSymptoms = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.symptoms = new System.Windows.Forms.Label();
            this.diastolicReadingField = new System.Windows.Forms.Label();
            this.systolicReadingField = new System.Windows.Forms.Label();
            this.pulseField = new System.Windows.Forms.Label();
            this.bodyTemperatureField = new System.Windows.Forms.Label();
            this.bloodPressureField = new System.Windows.Forms.Label();
            this.diastolicReadingLabel = new System.Windows.Forms.Label();
            this.ststolicReadingLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bodyTempLabel = new System.Windows.Forms.Label();
            this.bloodPressureLabel = new System.Windows.Forms.Label();
            this.recordDataButton = new System.Windows.Forms.Button();
            this.generalAppointmentInformationGroupBox.SuspendLayout();
            this.diagnosisInfoGroupBox.SuspendLayout();
            this.testInformationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderedTestDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testResultsDataGridView)).BeginInit();
            this.routineChecksGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientSymptomsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // appointmentIDLabel
            // 
            this.appointmentIDLabel.AutoSize = true;
            this.appointmentIDLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appointmentIDLabel.Location = new System.Drawing.Point(31, 45);
            this.appointmentIDLabel.Name = "appointmentIDLabel";
            this.appointmentIDLabel.Size = new System.Drawing.Size(109, 16);
            this.appointmentIDLabel.TabIndex = 0;
            this.appointmentIDLabel.Text = "Appointment ID";
            // 
            // appointmentInfo_paientID
            // 
            this.appointmentInfo_paientID.AutoSize = true;
            this.appointmentInfo_paientID.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appointmentInfo_paientID.Location = new System.Drawing.Point(71, 72);
            this.appointmentInfo_paientID.Name = "appointmentInfo_paientID";
            this.appointmentInfo_paientID.Size = new System.Drawing.Size(69, 16);
            this.appointmentInfo_paientID.TabIndex = 1;
            this.appointmentInfo_paientID.Text = "Patient ID";
            // 
            // dataLabel
            // 
            this.dataLabel.AutoSize = true;
            this.dataLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataLabel.Location = new System.Drawing.Point(102, 99);
            this.dataLabel.Name = "dataLabel";
            this.dataLabel.Size = new System.Drawing.Size(38, 16);
            this.dataLabel.TabIndex = 2;
            this.dataLabel.Text = "Date";
            // 
            // reasonForVisitLabel
            // 
            this.reasonForVisitLabel.AutoSize = true;
            this.reasonForVisitLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reasonForVisitLabel.Location = new System.Drawing.Point(33, 126);
            this.reasonForVisitLabel.Name = "reasonForVisitLabel";
            this.reasonForVisitLabel.Size = new System.Drawing.Size(107, 16);
            this.reasonForVisitLabel.TabIndex = 3;
            this.reasonForVisitLabel.Text = "Reason for Visit";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(90, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Doctor";
            // 
            // generalAppointmentInformationGroupBox
            // 
            this.generalAppointmentInformationGroupBox.Controls.Add(this.doctorNameFeild);
            this.generalAppointmentInformationGroupBox.Controls.Add(this.reasonForVisitFeild);
            this.generalAppointmentInformationGroupBox.Controls.Add(this.appointmentDateFeild);
            this.generalAppointmentInformationGroupBox.Controls.Add(this.patientIDFeild);
            this.generalAppointmentInformationGroupBox.Controls.Add(this.appointmentIDFeild);
            this.generalAppointmentInformationGroupBox.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generalAppointmentInformationGroupBox.Location = new System.Drawing.Point(12, 11);
            this.generalAppointmentInformationGroupBox.Name = "generalAppointmentInformationGroupBox";
            this.generalAppointmentInformationGroupBox.Size = new System.Drawing.Size(516, 193);
            this.generalAppointmentInformationGroupBox.TabIndex = 5;
            this.generalAppointmentInformationGroupBox.TabStop = false;
            this.generalAppointmentInformationGroupBox.Text = "General Information";
            // 
            // doctorNameFeild
            // 
            this.doctorNameFeild.AutoSize = true;
            this.doctorNameFeild.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doctorNameFeild.Location = new System.Drawing.Point(170, 145);
            this.doctorNameFeild.Name = "doctorNameFeild";
            this.doctorNameFeild.Size = new System.Drawing.Size(80, 16);
            this.doctorNameFeild.TabIndex = 4;
            this.doctorNameFeild.Text = "Doctor Name";
            // 
            // reasonForVisitFeild
            // 
            this.reasonForVisitFeild.AutoSize = true;
            this.reasonForVisitFeild.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reasonForVisitFeild.Location = new System.Drawing.Point(170, 120);
            this.reasonForVisitFeild.Name = "reasonForVisitFeild";
            this.reasonForVisitFeild.Size = new System.Drawing.Size(92, 16);
            this.reasonForVisitFeild.TabIndex = 3;
            this.reasonForVisitFeild.Text = "Reason For Visist";
            // 
            // appointmentDateFeild
            // 
            this.appointmentDateFeild.AutoSize = true;
            this.appointmentDateFeild.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appointmentDateFeild.Location = new System.Drawing.Point(170, 91);
            this.appointmentDateFeild.Name = "appointmentDateFeild";
            this.appointmentDateFeild.Size = new System.Drawing.Size(109, 16);
            this.appointmentDateFeild.TabIndex = 2;
            this.appointmentDateFeild.Text = "Appointment Date";
            // 
            // patientIDFeild
            // 
            this.patientIDFeild.AutoSize = true;
            this.patientIDFeild.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientIDFeild.Location = new System.Drawing.Point(170, 64);
            this.patientIDFeild.Name = "patientIDFeild";
            this.patientIDFeild.Size = new System.Drawing.Size(61, 16);
            this.patientIDFeild.TabIndex = 1;
            this.patientIDFeild.Text = "Patient ID";
            // 
            // appointmentIDFeild
            // 
            this.appointmentIDFeild.AutoSize = true;
            this.appointmentIDFeild.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appointmentIDFeild.Location = new System.Drawing.Point(170, 34);
            this.appointmentIDFeild.Name = "appointmentIDFeild";
            this.appointmentIDFeild.Size = new System.Drawing.Size(93, 16);
            this.appointmentIDFeild.TabIndex = 0;
            this.appointmentIDFeild.Text = "Appointment ID";
            // 
            // diagnosisInfoGroupBox
            // 
            this.diagnosisInfoGroupBox.Controls.Add(this.finalDiagnosisTextBox);
            this.diagnosisInfoGroupBox.Controls.Add(this.initialDiagnosisTextBox);
            this.diagnosisInfoGroupBox.Controls.Add(this.finalDiagnosisSubmitButton);
            this.diagnosisInfoGroupBox.Controls.Add(this.submitInitialDiagnosisButton);
            this.diagnosisInfoGroupBox.Controls.Add(this.finalDiagnosisLabel);
            this.diagnosisInfoGroupBox.Controls.Add(this.initialDiagnosisLabel);
            this.diagnosisInfoGroupBox.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diagnosisInfoGroupBox.Location = new System.Drawing.Point(557, 15);
            this.diagnosisInfoGroupBox.Name = "diagnosisInfoGroupBox";
            this.diagnosisInfoGroupBox.Size = new System.Drawing.Size(537, 134);
            this.diagnosisInfoGroupBox.TabIndex = 6;
            this.diagnosisInfoGroupBox.TabStop = false;
            this.diagnosisInfoGroupBox.Text = "Diagnosis Information";
            // 
            // finalDiagnosisTextBox
            // 
            this.finalDiagnosisTextBox.Location = new System.Drawing.Point(166, 79);
            this.finalDiagnosisTextBox.Name = "finalDiagnosisTextBox";
            this.finalDiagnosisTextBox.Size = new System.Drawing.Size(100, 21);
            this.finalDiagnosisTextBox.TabIndex = 10;
            // 
            // initialDiagnosisTextBox
            // 
            this.initialDiagnosisTextBox.Location = new System.Drawing.Point(166, 42);
            this.initialDiagnosisTextBox.Name = "initialDiagnosisTextBox";
            this.initialDiagnosisTextBox.Size = new System.Drawing.Size(100, 21);
            this.initialDiagnosisTextBox.TabIndex = 9;
            // 
            // finalDiagnosisSubmitButton
            // 
            this.finalDiagnosisSubmitButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finalDiagnosisSubmitButton.Location = new System.Drawing.Point(316, 81);
            this.finalDiagnosisSubmitButton.Name = "finalDiagnosisSubmitButton";
            this.finalDiagnosisSubmitButton.Size = new System.Drawing.Size(75, 23);
            this.finalDiagnosisSubmitButton.TabIndex = 8;
            this.finalDiagnosisSubmitButton.Text = "Submit";
            this.finalDiagnosisSubmitButton.UseVisualStyleBackColor = true;
            this.finalDiagnosisSubmitButton.Click += new System.EventHandler(this.finalDiagnosisSubmitButton_Click);
            // 
            // submitInitialDiagnosisButton
            // 
            this.submitInitialDiagnosisButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitInitialDiagnosisButton.Location = new System.Drawing.Point(316, 40);
            this.submitInitialDiagnosisButton.Name = "submitInitialDiagnosisButton";
            this.submitInitialDiagnosisButton.Size = new System.Drawing.Size(75, 23);
            this.submitInitialDiagnosisButton.TabIndex = 7;
            this.submitInitialDiagnosisButton.Text = "Submit";
            this.submitInitialDiagnosisButton.UseVisualStyleBackColor = true;
            this.submitInitialDiagnosisButton.Click += new System.EventHandler(this.submitInitialDiagnosisButton_Click);
            // 
            // finalDiagnosisLabel
            // 
            this.finalDiagnosisLabel.AutoSize = true;
            this.finalDiagnosisLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finalDiagnosisLabel.Location = new System.Drawing.Point(34, 83);
            this.finalDiagnosisLabel.Name = "finalDiagnosisLabel";
            this.finalDiagnosisLabel.Size = new System.Drawing.Size(106, 16);
            this.finalDiagnosisLabel.TabIndex = 1;
            this.finalDiagnosisLabel.Text = "Final Diagnosis";
            // 
            // initialDiagnosisLabel
            // 
            this.initialDiagnosisLabel.AutoSize = true;
            this.initialDiagnosisLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initialDiagnosisLabel.Location = new System.Drawing.Point(28, 45);
            this.initialDiagnosisLabel.Name = "initialDiagnosisLabel";
            this.initialDiagnosisLabel.Size = new System.Drawing.Size(112, 16);
            this.initialDiagnosisLabel.TabIndex = 0;
            this.initialDiagnosisLabel.Text = "Initial Diagnosis";
            // 
            // testInformationGroupBox
            // 
            this.testInformationGroupBox.Controls.Add(this.orderedTestDataGridView);
            this.testInformationGroupBox.Controls.Add(this.testResultsDataGridView);
            this.testInformationGroupBox.Controls.Add(this.testResultsLabel);
            this.testInformationGroupBox.Controls.Add(this.avalibleTestLabel);
            this.testInformationGroupBox.Controls.Add(this.orderTestButton);
            this.testInformationGroupBox.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testInformationGroupBox.Location = new System.Drawing.Point(557, 165);
            this.testInformationGroupBox.Name = "testInformationGroupBox";
            this.testInformationGroupBox.Size = new System.Drawing.Size(537, 377);
            this.testInformationGroupBox.TabIndex = 7;
            this.testInformationGroupBox.TabStop = false;
            this.testInformationGroupBox.Text = "Test Information";
            // 
            // orderedTestDataGridView
            // 
            this.orderedTestDataGridView.AllowUserToAddRows = false;
            this.orderedTestDataGridView.AllowUserToDeleteRows = false;
            this.orderedTestDataGridView.AllowUserToResizeColumns = false;
            this.orderedTestDataGridView.AllowUserToResizeRows = false;
            this.orderedTestDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderedTestDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.testCheckBox,
            this.test});
            this.orderedTestDataGridView.Location = new System.Drawing.Point(20, 42);
            this.orderedTestDataGridView.Name = "orderedTestDataGridView";
            this.orderedTestDataGridView.ReadOnly = true;
            this.orderedTestDataGridView.RowHeadersVisible = false;
            this.orderedTestDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.orderedTestDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.orderedTestDataGridView.Size = new System.Drawing.Size(260, 139);
            this.orderedTestDataGridView.TabIndex = 5;
            this.orderedTestDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.orderedTestDataGridView_CellContentClick);
            // 
            // testCheckBox
            // 
            this.testCheckBox.HeaderText = "";
            this.testCheckBox.Name = "testCheckBox";
            this.testCheckBox.ReadOnly = true;
            this.testCheckBox.Width = 25;
            // 
            // test
            // 
            this.test.HeaderText = "Test";
            this.test.Name = "test";
            this.test.ReadOnly = true;
            this.test.Width = 250;
            // 
            // testResultsDataGridView
            // 
            this.testResultsDataGridView.AllowUserToAddRows = false;
            this.testResultsDataGridView.AllowUserToDeleteRows = false;
            this.testResultsDataGridView.AllowUserToResizeColumns = false;
            this.testResultsDataGridView.AllowUserToResizeRows = false;
            this.testResultsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.testResultsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.testName,
            this.testResult,
            this.datePreformed});
            this.testResultsDataGridView.Location = new System.Drawing.Point(20, 206);
            this.testResultsDataGridView.Name = "testResultsDataGridView";
            this.testResultsDataGridView.ReadOnly = true;
            this.testResultsDataGridView.RowHeadersVisible = false;
            this.testResultsDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.testResultsDataGridView.Size = new System.Drawing.Size(480, 156);
            this.testResultsDataGridView.TabIndex = 4;
            // 
            // testName
            // 
            this.testName.HeaderText = "Test";
            this.testName.Name = "testName";
            this.testName.ReadOnly = true;
            this.testName.Width = 170;
            // 
            // testResult
            // 
            this.testResult.HeaderText = "Result";
            this.testResult.Name = "testResult";
            this.testResult.ReadOnly = true;
            this.testResult.Width = 170;
            // 
            // datePreformed
            // 
            this.datePreformed.HeaderText = "Date";
            this.datePreformed.Name = "datePreformed";
            this.datePreformed.ReadOnly = true;
            this.datePreformed.Width = 140;
            // 
            // testResultsLabel
            // 
            this.testResultsLabel.AutoSize = true;
            this.testResultsLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testResultsLabel.Location = new System.Drawing.Point(22, 190);
            this.testResultsLabel.Name = "testResultsLabel";
            this.testResultsLabel.Size = new System.Drawing.Size(67, 15);
            this.testResultsLabel.TabIndex = 2;
            this.testResultsLabel.Text = "Test Results";
            // 
            // avalibleTestLabel
            // 
            this.avalibleTestLabel.AutoSize = true;
            this.avalibleTestLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avalibleTestLabel.Location = new System.Drawing.Point(17, 26);
            this.avalibleTestLabel.Name = "avalibleTestLabel";
            this.avalibleTestLabel.Size = new System.Drawing.Size(74, 15);
            this.avalibleTestLabel.TabIndex = 1;
            this.avalibleTestLabel.Text = "Avalible Test";
            // 
            // orderTestButton
            // 
            this.orderTestButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderTestButton.Location = new System.Drawing.Point(334, 93);
            this.orderTestButton.Name = "orderTestButton";
            this.orderTestButton.Size = new System.Drawing.Size(128, 36);
            this.orderTestButton.TabIndex = 0;
            this.orderTestButton.Text = "Order Test";
            this.orderTestButton.UseVisualStyleBackColor = true;
            this.orderTestButton.Click += new System.EventHandler(this.orderTestButton_Click);
            // 
            // routineChecksGroupBox
            // 
            this.routineChecksGroupBox.Controls.Add(this.patientSymptomsDataGridView);
            this.routineChecksGroupBox.Controls.Add(this.symptoms);
            this.routineChecksGroupBox.Controls.Add(this.diastolicReadingField);
            this.routineChecksGroupBox.Controls.Add(this.systolicReadingField);
            this.routineChecksGroupBox.Controls.Add(this.pulseField);
            this.routineChecksGroupBox.Controls.Add(this.bodyTemperatureField);
            this.routineChecksGroupBox.Controls.Add(this.bloodPressureField);
            this.routineChecksGroupBox.Controls.Add(this.diastolicReadingLabel);
            this.routineChecksGroupBox.Controls.Add(this.ststolicReadingLabel);
            this.routineChecksGroupBox.Controls.Add(this.label1);
            this.routineChecksGroupBox.Controls.Add(this.bodyTempLabel);
            this.routineChecksGroupBox.Controls.Add(this.bloodPressureLabel);
            this.routineChecksGroupBox.Controls.Add(this.recordDataButton);
            this.routineChecksGroupBox.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.routineChecksGroupBox.Location = new System.Drawing.Point(12, 222);
            this.routineChecksGroupBox.Name = "routineChecksGroupBox";
            this.routineChecksGroupBox.Size = new System.Drawing.Size(516, 320);
            this.routineChecksGroupBox.TabIndex = 8;
            this.routineChecksGroupBox.TabStop = false;
            this.routineChecksGroupBox.Text = "Routine Checks";
            // 
            // patientSymptomsDataGridView
            // 
            this.patientSymptomsDataGridView.AllowUserToAddRows = false;
            this.patientSymptomsDataGridView.AllowUserToDeleteRows = false;
            this.patientSymptomsDataGridView.AllowUserToResizeColumns = false;
            this.patientSymptomsDataGridView.AllowUserToResizeRows = false;
            this.patientSymptomsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patientSymptomsDataGridView.ColumnHeadersVisible = false;
            this.patientSymptomsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.patientSymptoms});
            this.patientSymptomsDataGridView.Location = new System.Drawing.Point(291, 49);
            this.patientSymptomsDataGridView.Name = "patientSymptomsDataGridView";
            this.patientSymptomsDataGridView.ReadOnly = true;
            this.patientSymptomsDataGridView.RowHeadersVisible = false;
            this.patientSymptomsDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.patientSymptomsDataGridView.Size = new System.Drawing.Size(204, 139);
            this.patientSymptomsDataGridView.TabIndex = 15;
            // 
            // patientSymptoms
            // 
            this.patientSymptoms.HeaderText = "Symptoms";
            this.patientSymptoms.Name = "patientSymptoms";
            this.patientSymptoms.ReadOnly = true;
            this.patientSymptoms.Width = 200;
            // 
            // symptoms
            // 
            this.symptoms.AutoSize = true;
            this.symptoms.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symptoms.Location = new System.Drawing.Point(288, 30);
            this.symptoms.Name = "symptoms";
            this.symptoms.Size = new System.Drawing.Size(74, 16);
            this.symptoms.TabIndex = 14;
            this.symptoms.Text = "Symptoms";
            // 
            // diastolicReadingField
            // 
            this.diastolicReadingField.AutoSize = true;
            this.diastolicReadingField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diastolicReadingField.Location = new System.Drawing.Point(164, 175);
            this.diastolicReadingField.Name = "diastolicReadingField";
            this.diastolicReadingField.Size = new System.Drawing.Size(105, 16);
            this.diastolicReadingField.TabIndex = 13;
            this.diastolicReadingField.Text = "Not Recorded Yet";
            // 
            // systolicReadingField
            // 
            this.systolicReadingField.AutoSize = true;
            this.systolicReadingField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.systolicReadingField.Location = new System.Drawing.Point(164, 141);
            this.systolicReadingField.Name = "systolicReadingField";
            this.systolicReadingField.Size = new System.Drawing.Size(105, 16);
            this.systolicReadingField.TabIndex = 12;
            this.systolicReadingField.Text = "Not Recorded Yet";
            // 
            // pulseField
            // 
            this.pulseField.AutoSize = true;
            this.pulseField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pulseField.Location = new System.Drawing.Point(164, 106);
            this.pulseField.Name = "pulseField";
            this.pulseField.Size = new System.Drawing.Size(105, 16);
            this.pulseField.TabIndex = 11;
            this.pulseField.Text = "Not Recorded Yet";
            // 
            // bodyTemperatureField
            // 
            this.bodyTemperatureField.AutoSize = true;
            this.bodyTemperatureField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bodyTemperatureField.Location = new System.Drawing.Point(164, 70);
            this.bodyTemperatureField.Name = "bodyTemperatureField";
            this.bodyTemperatureField.Size = new System.Drawing.Size(105, 16);
            this.bodyTemperatureField.TabIndex = 10;
            this.bodyTemperatureField.Text = "Not Recorded Yet";
            // 
            // bloodPressureField
            // 
            this.bloodPressureField.AutoSize = true;
            this.bloodPressureField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bloodPressureField.Location = new System.Drawing.Point(164, 39);
            this.bloodPressureField.Name = "bloodPressureField";
            this.bloodPressureField.Size = new System.Drawing.Size(105, 16);
            this.bloodPressureField.TabIndex = 7;
            this.bloodPressureField.Text = "Not Recorded Yet";
            // 
            // diastolicReadingLabel
            // 
            this.diastolicReadingLabel.AutoSize = true;
            this.diastolicReadingLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diastolicReadingLabel.Location = new System.Drawing.Point(17, 172);
            this.diastolicReadingLabel.Name = "diastolicReadingLabel";
            this.diastolicReadingLabel.Size = new System.Drawing.Size(123, 16);
            this.diastolicReadingLabel.TabIndex = 9;
            this.diastolicReadingLabel.Text = "Diastolic Reading";
            // 
            // ststolicReadingLabel
            // 
            this.ststolicReadingLabel.AutoSize = true;
            this.ststolicReadingLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ststolicReadingLabel.Location = new System.Drawing.Point(24, 138);
            this.ststolicReadingLabel.Name = "ststolicReadingLabel";
            this.ststolicReadingLabel.Size = new System.Drawing.Size(116, 16);
            this.ststolicReadingLabel.TabIndex = 8;
            this.ststolicReadingLabel.Text = "Systolic Reading";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(99, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Pulse";
            // 
            // bodyTempLabel
            // 
            this.bodyTempLabel.AutoSize = true;
            this.bodyTempLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bodyTempLabel.Location = new System.Drawing.Point(13, 70);
            this.bodyTempLabel.Name = "bodyTempLabel";
            this.bodyTempLabel.Size = new System.Drawing.Size(127, 16);
            this.bodyTempLabel.TabIndex = 6;
            this.bodyTempLabel.Text = "Body Temperature";
            // 
            // bloodPressureLabel
            // 
            this.bloodPressureLabel.AutoSize = true;
            this.bloodPressureLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bloodPressureLabel.Location = new System.Drawing.Point(38, 36);
            this.bloodPressureLabel.Name = "bloodPressureLabel";
            this.bloodPressureLabel.Size = new System.Drawing.Size(102, 16);
            this.bloodPressureLabel.TabIndex = 1;
            this.bloodPressureLabel.Text = "Blood Pressure";
            // 
            // recordDataButton
            // 
            this.recordDataButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recordDataButton.Location = new System.Drawing.Point(184, 232);
            this.recordDataButton.Name = "recordDataButton";
            this.recordDataButton.Size = new System.Drawing.Size(151, 40);
            this.recordDataButton.TabIndex = 0;
            this.recordDataButton.Text = "Record Data";
            this.recordDataButton.UseVisualStyleBackColor = true;
            this.recordDataButton.Click += new System.EventHandler(this.recordDataButton_Click);
            // 
            // PatientAppointmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1106, 562);
            this.Controls.Add(this.routineChecksGroupBox);
            this.Controls.Add(this.testInformationGroupBox);
            this.Controls.Add(this.diagnosisInfoGroupBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.reasonForVisitLabel);
            this.Controls.Add(this.dataLabel);
            this.Controls.Add(this.appointmentInfo_paientID);
            this.Controls.Add(this.appointmentIDLabel);
            this.Controls.Add(this.generalAppointmentInformationGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PatientAppointmentForm";
            this.Text = "Appointment Information";
            this.generalAppointmentInformationGroupBox.ResumeLayout(false);
            this.generalAppointmentInformationGroupBox.PerformLayout();
            this.diagnosisInfoGroupBox.ResumeLayout(false);
            this.diagnosisInfoGroupBox.PerformLayout();
            this.testInformationGroupBox.ResumeLayout(false);
            this.testInformationGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderedTestDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testResultsDataGridView)).EndInit();
            this.routineChecksGroupBox.ResumeLayout(false);
            this.routineChecksGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientSymptomsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label appointmentIDLabel;
        private System.Windows.Forms.Label appointmentInfo_paientID;
        private System.Windows.Forms.Label dataLabel;
        private System.Windows.Forms.Label reasonForVisitLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox generalAppointmentInformationGroupBox;
        private System.Windows.Forms.GroupBox diagnosisInfoGroupBox;
        private System.Windows.Forms.GroupBox testInformationGroupBox;
        private System.Windows.Forms.Label finalDiagnosisLabel;
        private System.Windows.Forms.Label initialDiagnosisLabel;
        private System.Windows.Forms.DataGridView orderedTestDataGridView;
        private System.Windows.Forms.DataGridView testResultsDataGridView;
        private System.Windows.Forms.Label testResultsLabel;
        private System.Windows.Forms.Label avalibleTestLabel;
        private System.Windows.Forms.Button orderTestButton;
        private System.Windows.Forms.GroupBox routineChecksGroupBox;
        private System.Windows.Forms.Label bodyTempLabel;
        private System.Windows.Forms.Label bloodPressureLabel;
        private System.Windows.Forms.Button recordDataButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ststolicReadingLabel;
        private System.Windows.Forms.Label diastolicReadingLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn testName;
        private System.Windows.Forms.DataGridViewTextBoxColumn testResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn datePreformed;
        private System.Windows.Forms.DataGridViewCheckBoxColumn testCheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn test;
        private System.Windows.Forms.Label doctorNameFeild;
        private System.Windows.Forms.Label reasonForVisitFeild;
        private System.Windows.Forms.Label appointmentDateFeild;
        private System.Windows.Forms.Label patientIDFeild;
        private System.Windows.Forms.Label appointmentIDFeild;
        private System.Windows.Forms.Label diastolicReadingField;
        private System.Windows.Forms.Label systolicReadingField;
        private System.Windows.Forms.Label pulseField;
        private System.Windows.Forms.Label bodyTemperatureField;
        private System.Windows.Forms.Label bloodPressureField;
        private System.Windows.Forms.TextBox finalDiagnosisTextBox;
        private System.Windows.Forms.TextBox initialDiagnosisTextBox;
        private System.Windows.Forms.Button finalDiagnosisSubmitButton;
        private System.Windows.Forms.Button submitInitialDiagnosisButton;
        private System.Windows.Forms.Label symptoms;
        private System.Windows.Forms.DataGridView patientSymptomsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn patientSymptoms;
    }
}