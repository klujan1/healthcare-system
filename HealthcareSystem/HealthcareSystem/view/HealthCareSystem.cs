﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using HealthcareSystem.controller;
using HealthcareSystem.model;
using HealthcareSystem.view;
using HealthCareSystem.controller;
using HealthCareSystem.model;

namespace HealthCareSystem.view
{
    public partial class HealthCareSystemForm : Form
    {
        public Patient SelectedPatient { get; set; }

        private readonly int currentUserID;
        private int searchByDate;
        private IList<Patient> patients;

        public HealthCareSystemForm()
        {
            InitializeComponent();
            patientDataGridView.Columns.Add("patientID", "Patient ID");
            patientDataGridView.Columns.Add("patientName", "Patient Name");
            searchByDateRadioButton.Checked = true;
        }

        public HealthCareSystemForm(int currentUserID, bool admin)
        {
            this.currentUserID = currentUserID;
            InitializeComponent();
            CheckIfAdmin(admin);
    
            var currentEmployee = AccountRetrieval.GetEmployeeInfo(this.currentUserID);
            if (currentEmployee == null)
            {
                MessageBox.Show("Employee could not be found. Something went wrong.");
                Close();
                return;
            }

            if (admin)
            {
                employeeNameLabel.Text = currentEmployee.FirstName + " " + currentEmployee.LastName + " (Admin)";
            }
            else
            {
                employeeNameLabel.Text = currentEmployee.FirstName + " " + currentEmployee.LastName;
                this.employeeTabControl.TabPages.Remove(adminTabPage);
            }
            employeeIDLabel.Text = currentEmployee.Id.ToString();
            patientFirstNameTextBox.Enabled = true;
            patientLastNameTextBox.Enabled = true;
            patientDOBDateTimePicker.Enabled = true;

            var controller = new PatientController();

            patients = controller.GetAllPatients();
            PopulatePatientTable();
            SelectedPatient = patients[0];
            SetSelectedPatientInformation();
            PopulateSelectedPatientAppointments(SelectedPatient.Id);

            TestController controller2 = new TestController();

            if (this.patientAppointmentDataGridView.RowCount == 0)
            {
                viewAppointmentButton.Enabled = false;
            }
        }

        private void CheckIfAdmin(bool admin)
        {
            if (!admin)
            {
                adminTabPage.Text = "Access Denied";
            }
            else
            {
                adminTabPage.Text = "Administrator";
            }
        }

        private void PopulatePatientTable()
        {
            foreach (var patient in patients)
            {
                var patientName = patient.FirstName + " " + patient.Midinit + ". " + patient.LastName;
                patientDataGridView.Rows.Add(patient.Id, patientName);
            }
        }

        private void newPatientButton_Click(object sender, EventArgs e)
        {

            var form = new NewPatientForm(currentUserID, this);
            form.Show();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            Close();
            var logIn = new LoginForm();
            logIn.Show();
        }

        private void patientSearchButton_Click(object sender, EventArgs e)
        {
            patientDataGridView.Rows.Clear();
            var search = new SearchController();


            if (searchByDate == 0)
            {
                if (patientFirstNameTextBox.Text.Length != 0 && patientLastNameTextBox.Text.Length != 0)
                {
                    patients = search.SearchByName(patientFirstNameTextBox.Text,
                        patientLastNameTextBox.Text);
                }
                else if (patientFirstNameTextBox.Text.Length != 0 && patientLastNameTextBox.Text.Length == 0)
                {
                    patients = search.SearchByName(patientFirstNameTextBox.Text, "");
                }

                else
                {
                    patients = search.SearchByName("", patientLastNameTextBox.Text);
                }
            }
            else if (searchByDate == 1)
            {
                patients = search.SearchByBirthDate(patientDOBDateTimePicker.Value);
            }

            else
            {
                patients = search.SearchByBirthDateAndName(patientDOBDateTimePicker.Value, patientFirstNameTextBox.Text,
                    patientLastNameTextBox.Text);
            }

            if (patients == null)
            {
                MessageBox.Show("No patients were found!");
                return;
            }
            foreach (var patient in patients)
            {
                var patientName = patient.FirstName + " " + patient.Midinit + ". " + patient.LastName;
                patientDataGridView.Rows.Add(patient.Id, patientName);
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            patientFirstNameTextBox.Enabled = true;
            patientLastNameTextBox.Enabled = true;
            patientDOBDateTimePicker.Enabled = false;
            searchByDate = 0;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            patientFirstNameTextBox.Enabled = false;
            patientLastNameTextBox.Enabled = false;
            patientDOBDateTimePicker.Enabled = true;
            searchByDate = 1;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            patientFirstNameTextBox.Enabled = true;
            patientLastNameTextBox.Enabled = true;
            patientDOBDateTimePicker.Enabled = true;
            searchByDate = 2;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void scheduleAppointmentButton_Click(object sender, EventArgs e)
        {
            var form = new NewAppointmentForm(SelectedPatient);
            form.Show();
        }

        private void sqlTerminalSubmitButton_Click(object sender, EventArgs e)
        {
            this.sqlResultsDataView.Columns.Clear();
            this.sqlResultsDataView.Rows.Clear();
            DataTable table = null;
            AdminSQL terminal = new AdminSQL();
            try
            {
                 terminal.SubmitSQL(this.sqlTerminalRichTextBox.Text);
            }
            catch
            {
                MessageBox.Show("The query failed. Please check and retry your query.");
            }

            var columns = terminal.getColumnNames();

            if (columns == null)
            {
                return;
            }

            foreach (var column in columns)
            {
                sqlResultsDataView.Columns.Add(column.ToString().Replace(" ",""), column.ToString());
            }

            int count = 0;
            List<string> fullRow = new List<string>();
            foreach (var row in terminal.getRows())
            {
                fullRow.Add(row);
                if (count == terminal.getNumberOfColumns() - 1)
                {
                    sqlResultsDataView.Rows.Add(fullRow.ToArray());
                    fullRow.Clear();
                    count = 0;
                }
                else
                {
                    count += 1;
                }
            }
        }

        private void patientDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            patientAppointmentDataGridView.Rows.Clear();
            var select = patientDataGridView.SelectedRows[0].Cells[0];
            foreach (var patient in patients)
            {
                if (patient.Id == int.Parse(select.Value.ToString()))
                {
                    SelectedPatient = patient;
                }
            }

            PopulateSelectedPatientAppointments(int.Parse(select.Value.ToString()));

            if (this.patientAppointmentDataGridView.RowCount == 0)
            {
                viewAppointmentButton.Enabled = false;
            }
            else
            {
                viewAppointmentButton.Enabled = true;
            }
        }

        private void SetSelectedPatientInformation()
        {
            patientNameField.Text = SelectedPatient.FirstName + " " + SelectedPatient.Midinit + " " +
                                    SelectedPatient.LastName;
            patientIDField.Text = SelectedPatient.Id.ToString();
            patientDOBField.Text = SelectedPatient.DateOfBirth.Date.ToLongDateString();
            patientAddressField.Text = SelectedPatient.Street + " " + SelectedPatient.City + " " + SelectedPatient.State +
                                       " " + SelectedPatient.Zipcode;
            patientPhoneNumberField.Text = SelectedPatient.PhoneNumber;
            patientGenderField.Text = SelectedPatient.Gender;
        }

        private void patientFirstNameTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (patientFirstNameTextBox.Text.Equals("First Name"))
            {
                patientFirstNameTextBox.Clear();
                patientFirstNameTextBox.ResetForeColor();
            }
        }

        private void patientLastNameTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (patientLastNameTextBox.Text.Equals("Last Name"))
            {
                patientLastNameTextBox.Clear();
                patientLastNameTextBox.ResetForeColor();
            }
        }

        private void patientFirstNameTextBox_Leave(object sender, EventArgs e)
        {
            if (patientFirstNameTextBox.Text.Trim() == "")
            {
                patientFirstNameTextBox.Text = "First Name";
                patientFirstNameTextBox.ForeColor = SystemColors.WindowFrame;
            }
        }

        private void patientLastNameTextBox_Leave(object sender, EventArgs e)
        {
            if (patientLastNameTextBox.Text.Trim() == "")
            {
                patientLastNameTextBox.Text = "Last Name";
                patientLastNameTextBox.ForeColor = SystemColors.WindowFrame;
            }
        }

        private void PopulateSelectedPatientAppointments(int patientID)
        {
            var search = new SearchController();
            var controller = new AppointmentController();
            var appointments = controller.GetAllAppointmentsByPatientId(patientID);

            foreach (var appointment in appointments)
            {
                patientAppointmentDataGridView.Rows.Add(appointment.Id, appointment.Date.Date.ToLongDateString(),
                    appointment.Date.ToLongTimeString(), appointment.Reason);
            }

            patientAppointmentDataGridView.Sort(patientAppointmentDataGridView.Columns[2], ListSortDirection.Descending);

            SetSelectedPatientInformation();
        }


        private void patientAppointmentDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            viewAppointmentButton.Enabled = true;
        }

        private void viewAppointmentButton_Click(object sender, EventArgs e)
        {
            var appointmentID = patientAppointmentDataGridView.SelectedRows[0].Cells[0].Value.ToString();

            var form = new PatientAppointmentForm(SelectedPatient.Id, int.Parse(appointmentID), this.currentUserID);
            form.Show();
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            sqlTerminalRichTextBox.Clear();
        }
    }
}
