﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HealthCareSystem.view;
using HealthCareSystem.controller;
using MySql.Data.MySqlClient;

namespace HealthCareSystem
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void logInButton_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (idField.Text.Length == 0 || passwordField.Text.Length == 0)
            {
                MessageBox.Show("Please enter both an ID and a Password.");
                return;
            }
            try
            {
                 id = Int32.Parse(idField.Text);
            }
            catch (Exception exception)
            {
                CatchBadPassword();
            }
            try
            {
                if (AccountVerificationController.CheckIDAndPassword
                    (id, passwordField.Text))
                {
                    this.Hide();
                    view.HealthCareSystemForm form = new view.HealthCareSystemForm(id,
                        AccountVerificationController.isAdmin(id));
                    form.Show();
                }
                else
                {
                    CatchBadPassword();
                }
            }
            catch (MySqlException exception)
            {
                MessageBox.Show(
                    "There is something wrong with your connection to the database. Please make sure you are connected and try again.");
            }
            catch (ObjectDisposedException exception)
            {
                idField.Clear();
                passwordField.Clear();
                this.Show();
            }
        }

        private void CatchBadPassword()
        {
            idField.Clear();
            passwordField.Clear();
            MessageBox.Show("Invalid ID or password. Try again.");
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
