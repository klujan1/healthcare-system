﻿using System;
using System.Linq;
using System.Windows.Forms;
using HealthCareSystem.view;
using HealthCareSystem.controller;
using HealthCareSystem.model;

namespace HealthCareSystem
{
    public partial class NewPatientForm : Form
    {
        private int currentUser;
        private HealthCareSystemForm form;
        public NewPatientForm(int currentUser, HealthCareSystemForm form)
        {
            InitializeComponent();
            this.currentUser = currentUser;
            this.form = form;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            var patient = new Patient();
            var failed = false;
            try
            {
                var firstName = ConvertString(firstNameField.Text);
                CheckIfStringContainsNonLetters(firstName);
                patient.FirstName = firstName;
                error1.Visible = false;
            }
            catch (Exception exception)
            {
                firstNameField.Clear();
                error1.Visible = true;
                failed = true;
            }
            try
            {
                if (midinitField.Text.Length != 1)
                {
                    throw new Exception();
                }
                CheckIfStringContainsNonLetters(midinitField.Text);
                patient.Midinit = midinitField.Text.ToUpper();
                error2.Visible = false;
            }
            catch (Exception exception)
            {
                midinitField.Clear();
                error2.Visible = true;
                failed = true;
            }
            try
            {
                var lastName = ConvertString(lastNameField.Text);
                CheckIfStringContainsNonLetters(lastName);
                patient.LastName = lastName;
                error3.Visible = false;
            }
            catch (Exception exception)
            {
                lastNameField.Clear();
                error3.Visible = true;
                failed = true;
            }
            var address = addressField.Text;
            var city = "";
            var zipCode = "";

            try
            {
                if (address.Length == 0)
                {
                    throw new Exception();
                }
                this.error4.Visible = false;
            }
            catch (Exception exception)
            {
                addressField.Clear();
                error4.Visible = true;
                failed = true;
            }

            try
            {
                city = ConvertString(cityTextBox.Text);
                CheckIfStringContainsNonLetters(city);
                error5.Visible = false;
            }

            catch (Exception exception)
            {
                cityTextBox.Clear();
                error5.Visible = true;
                failed = true;
            }

            try
            {
                CheckIfStringContainsNonLetters(zipCode);
                if (zipCodeTextBox.Text.All(char.IsLetter))
                {
                    throw new Exception();
                }
                zipCode = zipCodeTextBox.Text;
                error6.Visible = false;
            }
            catch (Exception exception)
            {
                zipCodeTextBox.Clear();
                error6.Visible = true;
                failed = true;
            }
            patient.Street = addressField.Text;
            patient.City = city;
            patient.State = comboBox1.Text;
            patient.Zipcode = zipCode;

            try
            {
                string phoneNumber = this.phoneNumberField.Text;
                if (phoneNumber.Length != 10)
                {
                    throw new Exception();
                }
                for (int i = 0; i < phoneNumber.Length; i++)
                {
                    Int16.Parse(phoneNumber[i].ToString());
                }
                patient.PhoneNumber = phoneNumber;
                this.error7.Visible = false;
            }
            catch (Exception exception)
            {
                phoneNumberField.Clear();
                this.error7.Visible = true;
                failed = true;
            }

            patient.Gender = this.genderComboBox.Text;

            patient.DateOfBirth = this.dobField.Value;
            if (failed)
            {
                return;
            }
            PatientController controller = new PatientController();
            controller.Add(patient);
            Close();
            MessageBox.Show("The patient has been added.");
            this.form.Close();
            HealthCareSystemForm care = new HealthCareSystemForm(this.currentUser, AccountVerificationController.isAdmin(currentUser));
            care.Show();
        }

        private static void CheckIfStringContainsNonLetters(string word)
        {
            if (!word.All(char.IsLetter))
            {
                throw new Exception();
            }
        }

        private string ConvertString(string word)
        {
            return word.First().ToString().ToUpper() + word.Substring(1);
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            HealthCareSystemForm care = new HealthCareSystemForm(this.currentUser, AccountVerificationController.isAdmin(currentUser));
            care.Show();
        }
    }
}