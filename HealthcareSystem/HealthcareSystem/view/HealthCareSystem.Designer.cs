﻿namespace HealthCareSystem.view
{
    partial class HealthCareSystemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HealthCareSystemForm));
            this.nameLabel = new System.Windows.Forms.Label();
            this.employeeInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.IDLabel = new System.Windows.Forms.Label();
            this.employeeIDLabel = new System.Windows.Forms.Label();
            this.employeeNameLabel = new System.Windows.Forms.Label();
            this.employeeTabControl = new System.Windows.Forms.TabControl();
            this.nurseTabPage = new System.Windows.Forms.TabPage();
            this.viewAppointmentButton = new System.Windows.Forms.Button();
            this.patientInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.patientGenderField = new System.Windows.Forms.Label();
            this.patientPhoneNumberField = new System.Windows.Forms.Label();
            this.patientAddressField = new System.Windows.Forms.Label();
            this.patientDOBField = new System.Windows.Forms.Label();
            this.patientIDField = new System.Windows.Forms.Label();
            this.patientNameField = new System.Windows.Forms.Label();
            this.patientGenderLabel = new System.Windows.Forms.Label();
            this.patientPhoneNumberLabel = new System.Windows.Forms.Label();
            this.patientAddressLabel = new System.Windows.Forms.Label();
            this.patientDOBLabel = new System.Windows.Forms.Label();
            this.patientIDLabel = new System.Windows.Forms.Label();
            this.patientNameLabel = new System.Windows.Forms.Label();
            this.appointmentLabel = new System.Windows.Forms.Label();
            this.patientAppointmentDataGridView = new System.Windows.Forms.DataGridView();
            this.appointmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.appointmentDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.appointmentTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.appointmentReason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patientDataGridView = new System.Windows.Forms.DataGridView();
            this.patientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scheduleAppointmentButton = new System.Windows.Forms.Button();
            this.newPatientButton = new System.Windows.Forms.Button();
            this.patientSearchGroupBox = new System.Windows.Forms.GroupBox();
            this.searchAllRadioButton = new System.Windows.Forms.RadioButton();
            this.searchByDateRadioButton = new System.Windows.Forms.RadioButton();
            this.searchByNameRadioButton = new System.Windows.Forms.RadioButton();
            this.patientSearchButton = new System.Windows.Forms.Button();
            this.patientDOBDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.patientFirstNameTextBox = new System.Windows.Forms.TextBox();
            this.dateOfBirthLabel = new System.Windows.Forms.Label();
            this.patientLastNameTextBox = new System.Windows.Forms.TextBox();
            this.adminTabPage = new System.Windows.Forms.TabPage();
            this.clearButton = new System.Windows.Forms.Button();
            this.sqlResultsLabel = new System.Windows.Forms.Label();
            this.sqlTerminalSubmitButton = new System.Windows.Forms.Button();
            this.sqlTerminalTextLabel = new System.Windows.Forms.Label();
            this.sqlTerminalRichTextBox = new System.Windows.Forms.RichTextBox();
            this.sqlResultsDataView = new System.Windows.Forms.DataGridView();
            this.logoutButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.employeeInfoGroupBox.SuspendLayout();
            this.employeeTabControl.SuspendLayout();
            this.nurseTabPage.SuspendLayout();
            this.patientInfoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientAppointmentDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientDataGridView)).BeginInit();
            this.patientSearchGroupBox.SuspendLayout();
            this.adminTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sqlResultsDataView)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(19, 30);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(53, 18);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Name";
            // 
            // employeeInfoGroupBox
            // 
            this.employeeInfoGroupBox.Controls.Add(this.IDLabel);
            this.employeeInfoGroupBox.Controls.Add(this.employeeIDLabel);
            this.employeeInfoGroupBox.Controls.Add(this.employeeNameLabel);
            this.employeeInfoGroupBox.Controls.Add(this.nameLabel);
            this.employeeInfoGroupBox.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeInfoGroupBox.Location = new System.Drawing.Point(12, 12);
            this.employeeInfoGroupBox.Name = "employeeInfoGroupBox";
            this.employeeInfoGroupBox.Size = new System.Drawing.Size(475, 65);
            this.employeeInfoGroupBox.TabIndex = 2;
            this.employeeInfoGroupBox.TabStop = false;
            this.employeeInfoGroupBox.Text = "Employee Information";
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDLabel.Location = new System.Drawing.Point(311, 30);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(23, 18);
            this.IDLabel.TabIndex = 7;
            this.IDLabel.Text = "ID";
            // 
            // employeeIDLabel
            // 
            this.employeeIDLabel.AutoSize = true;
            this.employeeIDLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeIDLabel.Location = new System.Drawing.Point(353, 30);
            this.employeeIDLabel.Name = "employeeIDLabel";
            this.employeeIDLabel.Size = new System.Drawing.Size(86, 20);
            this.employeeIDLabel.TabIndex = 3;
            this.employeeIDLabel.Text = "Id Number";
            // 
            // employeeNameLabel
            // 
            this.employeeNameLabel.AutoSize = true;
            this.employeeNameLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeNameLabel.Location = new System.Drawing.Point(86, 30);
            this.employeeNameLabel.Name = "employeeNameLabel";
            this.employeeNameLabel.Size = new System.Drawing.Size(163, 20);
            this.employeeNameLabel.TabIndex = 2;
            this.employeeNameLabel.Text = "First Name Last Name";
            // 
            // employeeTabControl
            // 
            this.employeeTabControl.Controls.Add(this.nurseTabPage);
            this.employeeTabControl.Controls.Add(this.adminTabPage);
            this.employeeTabControl.Location = new System.Drawing.Point(12, 83);
            this.employeeTabControl.Name = "employeeTabControl";
            this.employeeTabControl.SelectedIndex = 0;
            this.employeeTabControl.Size = new System.Drawing.Size(1106, 540);
            this.employeeTabControl.TabIndex = 3;
            // 
            // nurseTabPage
            // 
            this.nurseTabPage.Controls.Add(this.viewAppointmentButton);
            this.nurseTabPage.Controls.Add(this.patientInfoGroupBox);
            this.nurseTabPage.Controls.Add(this.patientDataGridView);
            this.nurseTabPage.Controls.Add(this.scheduleAppointmentButton);
            this.nurseTabPage.Controls.Add(this.newPatientButton);
            this.nurseTabPage.Controls.Add(this.patientSearchGroupBox);
            this.nurseTabPage.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nurseTabPage.Location = new System.Drawing.Point(4, 22);
            this.nurseTabPage.Name = "nurseTabPage";
            this.nurseTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.nurseTabPage.Size = new System.Drawing.Size(1098, 514);
            this.nurseTabPage.TabIndex = 0;
            this.nurseTabPage.Text = "Nurse";
            this.nurseTabPage.UseVisualStyleBackColor = true;
            // 
            // viewAppointmentButton
            // 
            this.viewAppointmentButton.Location = new System.Drawing.Point(871, 284);
            this.viewAppointmentButton.Name = "viewAppointmentButton";
            this.viewAppointmentButton.Size = new System.Drawing.Size(160, 33);
            this.viewAppointmentButton.TabIndex = 13;
            this.viewAppointmentButton.Text = "View Appointment";
            this.viewAppointmentButton.UseVisualStyleBackColor = true;
            this.viewAppointmentButton.Click += new System.EventHandler(this.viewAppointmentButton_Click);
            // 
            // patientInfoGroupBox
            // 
            this.patientInfoGroupBox.Controls.Add(this.patientGenderField);
            this.patientInfoGroupBox.Controls.Add(this.patientPhoneNumberField);
            this.patientInfoGroupBox.Controls.Add(this.patientAddressField);
            this.patientInfoGroupBox.Controls.Add(this.patientDOBField);
            this.patientInfoGroupBox.Controls.Add(this.patientIDField);
            this.patientInfoGroupBox.Controls.Add(this.patientNameField);
            this.patientInfoGroupBox.Controls.Add(this.patientGenderLabel);
            this.patientInfoGroupBox.Controls.Add(this.patientPhoneNumberLabel);
            this.patientInfoGroupBox.Controls.Add(this.patientAddressLabel);
            this.patientInfoGroupBox.Controls.Add(this.patientDOBLabel);
            this.patientInfoGroupBox.Controls.Add(this.patientIDLabel);
            this.patientInfoGroupBox.Controls.Add(this.patientNameLabel);
            this.patientInfoGroupBox.Controls.Add(this.appointmentLabel);
            this.patientInfoGroupBox.Controls.Add(this.patientAppointmentDataGridView);
            this.patientInfoGroupBox.Location = new System.Drawing.Point(343, 87);
            this.patientInfoGroupBox.Name = "patientInfoGroupBox";
            this.patientInfoGroupBox.Size = new System.Drawing.Size(522, 403);
            this.patientInfoGroupBox.TabIndex = 11;
            this.patientInfoGroupBox.TabStop = false;
            this.patientInfoGroupBox.Text = "Patient Information";
            // 
            // patientGenderField
            // 
            this.patientGenderField.AutoSize = true;
            this.patientGenderField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientGenderField.Location = new System.Drawing.Point(139, 174);
            this.patientGenderField.Name = "patientGenderField";
            this.patientGenderField.Size = new System.Drawing.Size(92, 16);
            this.patientGenderField.TabIndex = 13;
            this.patientGenderField.Text = "Patient Gender";
            // 
            // patientPhoneNumberField
            // 
            this.patientPhoneNumberField.AutoSize = true;
            this.patientPhoneNumberField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientPhoneNumberField.Location = new System.Drawing.Point(139, 145);
            this.patientPhoneNumberField.Name = "patientPhoneNumberField";
            this.patientPhoneNumberField.Size = new System.Drawing.Size(131, 16);
            this.patientPhoneNumberField.TabIndex = 12;
            this.patientPhoneNumberField.Text = "Patient Phone Number";
            // 
            // patientAddressField
            // 
            this.patientAddressField.AutoSize = true;
            this.patientAddressField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientAddressField.Location = new System.Drawing.Point(139, 116);
            this.patientAddressField.Name = "patientAddressField";
            this.patientAddressField.Size = new System.Drawing.Size(91, 16);
            this.patientAddressField.TabIndex = 11;
            this.patientAddressField.Text = "Patient Address";
            // 
            // patientDOBField
            // 
            this.patientDOBField.AutoSize = true;
            this.patientDOBField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientDOBField.Location = new System.Drawing.Point(139, 87);
            this.patientDOBField.Name = "patientDOBField";
            this.patientDOBField.Size = new System.Drawing.Size(115, 16);
            this.patientDOBField.TabIndex = 10;
            this.patientDOBField.Text = "Patient Date of Birth";
            // 
            // patientIDField
            // 
            this.patientIDField.AutoSize = true;
            this.patientIDField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientIDField.Location = new System.Drawing.Point(139, 58);
            this.patientIDField.Name = "patientIDField";
            this.patientIDField.Size = new System.Drawing.Size(61, 16);
            this.patientIDField.TabIndex = 9;
            this.patientIDField.Text = "Patient ID";
            // 
            // patientNameField
            // 
            this.patientNameField.AutoSize = true;
            this.patientNameField.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientNameField.Location = new System.Drawing.Point(139, 29);
            this.patientNameField.Name = "patientNameField";
            this.patientNameField.Size = new System.Drawing.Size(83, 16);
            this.patientNameField.TabIndex = 8;
            this.patientNameField.Text = "Patient Name";
            // 
            // patientGenderLabel
            // 
            this.patientGenderLabel.AutoSize = true;
            this.patientGenderLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientGenderLabel.Location = new System.Drawing.Point(59, 174);
            this.patientGenderLabel.Name = "patientGenderLabel";
            this.patientGenderLabel.Size = new System.Drawing.Size(48, 15);
            this.patientGenderLabel.TabIndex = 7;
            this.patientGenderLabel.Text = "Gender";
            // 
            // patientPhoneNumberLabel
            // 
            this.patientPhoneNumberLabel.AutoSize = true;
            this.patientPhoneNumberLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientPhoneNumberLabel.Location = new System.Drawing.Point(16, 145);
            this.patientPhoneNumberLabel.Name = "patientPhoneNumberLabel";
            this.patientPhoneNumberLabel.Size = new System.Drawing.Size(88, 15);
            this.patientPhoneNumberLabel.TabIndex = 6;
            this.patientPhoneNumberLabel.Text = "Phone Number";
            // 
            // patientAddressLabel
            // 
            this.patientAddressLabel.AutoSize = true;
            this.patientAddressLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientAddressLabel.Location = new System.Drawing.Point(56, 116);
            this.patientAddressLabel.Name = "patientAddressLabel";
            this.patientAddressLabel.Size = new System.Drawing.Size(50, 15);
            this.patientAddressLabel.TabIndex = 5;
            this.patientAddressLabel.Text = "Address";
            // 
            // patientDOBLabel
            // 
            this.patientDOBLabel.AutoSize = true;
            this.patientDOBLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientDOBLabel.Location = new System.Drawing.Point(33, 87);
            this.patientDOBLabel.Name = "patientDOBLabel";
            this.patientDOBLabel.Size = new System.Drawing.Size(72, 15);
            this.patientDOBLabel.TabIndex = 4;
            this.patientDOBLabel.Text = "Date of Birth";
            // 
            // patientIDLabel
            // 
            this.patientIDLabel.AutoSize = true;
            this.patientIDLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientIDLabel.Location = new System.Drawing.Point(88, 58);
            this.patientIDLabel.Name = "patientIDLabel";
            this.patientIDLabel.Size = new System.Drawing.Size(17, 15);
            this.patientIDLabel.TabIndex = 3;
            this.patientIDLabel.Text = "ID";
            // 
            // patientNameLabel
            // 
            this.patientNameLabel.AutoSize = true;
            this.patientNameLabel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientNameLabel.Location = new System.Drawing.Point(66, 29);
            this.patientNameLabel.Name = "patientNameLabel";
            this.patientNameLabel.Size = new System.Drawing.Size(39, 15);
            this.patientNameLabel.TabIndex = 2;
            this.patientNameLabel.Text = "Name";
            // 
            // appointmentLabel
            // 
            this.appointmentLabel.AutoSize = true;
            this.appointmentLabel.Location = new System.Drawing.Point(16, 213);
            this.appointmentLabel.Name = "appointmentLabel";
            this.appointmentLabel.Size = new System.Drawing.Size(92, 17);
            this.appointmentLabel.TabIndex = 1;
            this.appointmentLabel.Text = "Appointments";
            // 
            // patientAppointmentDataGridView
            // 
            this.patientAppointmentDataGridView.AllowUserToAddRows = false;
            this.patientAppointmentDataGridView.AllowUserToDeleteRows = false;
            this.patientAppointmentDataGridView.AllowUserToResizeColumns = false;
            this.patientAppointmentDataGridView.AllowUserToResizeRows = false;
            this.patientAppointmentDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patientAppointmentDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.appointmentID,
            this.appointmentDate,
            this.appointmentTime,
            this.appointmentReason});
            this.patientAppointmentDataGridView.Location = new System.Drawing.Point(19, 231);
            this.patientAppointmentDataGridView.Name = "patientAppointmentDataGridView";
            this.patientAppointmentDataGridView.ReadOnly = true;
            this.patientAppointmentDataGridView.RowHeadersVisible = false;
            this.patientAppointmentDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.patientAppointmentDataGridView.Size = new System.Drawing.Size(484, 150);
            this.patientAppointmentDataGridView.TabIndex = 0;
            this.patientAppointmentDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.patientAppointmentDataGridView_CellContentClick);
            // 
            // appointmentID
            // 
            this.appointmentID.HeaderText = "ID";
            this.appointmentID.Name = "appointmentID";
            this.appointmentID.ReadOnly = true;
            this.appointmentID.Width = 50;
            // 
            // appointmentDate
            // 
            this.appointmentDate.HeaderText = "Date";
            this.appointmentDate.Name = "appointmentDate";
            this.appointmentDate.ReadOnly = true;
            this.appointmentDate.Width = 175;
            // 
            // appointmentTime
            // 
            this.appointmentTime.HeaderText = "Time";
            this.appointmentTime.Name = "appointmentTime";
            this.appointmentTime.ReadOnly = true;
            // 
            // appointmentReason
            // 
            this.appointmentReason.HeaderText = "Reason";
            this.appointmentReason.Name = "appointmentReason";
            this.appointmentReason.ReadOnly = true;
            this.appointmentReason.Width = 175;
            // 
            // patientDataGridView
            // 
            this.patientDataGridView.AllowUserToAddRows = false;
            this.patientDataGridView.AllowUserToDeleteRows = false;
            this.patientDataGridView.AllowUserToResizeColumns = false;
            this.patientDataGridView.AllowUserToResizeRows = false;
            this.patientDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patientDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.patientID,
            this.patientName});
            this.patientDataGridView.Location = new System.Drawing.Point(16, 87);
            this.patientDataGridView.MultiSelect = false;
            this.patientDataGridView.Name = "patientDataGridView";
            this.patientDataGridView.ReadOnly = true;
            this.patientDataGridView.RowHeadersVisible = false;
            this.patientDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.patientDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.patientDataGridView.ShowRowErrors = false;
            this.patientDataGridView.Size = new System.Drawing.Size(294, 403);
            this.patientDataGridView.TabIndex = 3;
            this.patientDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.patientDataGridView_CellClick);
            // 
            // patientID
            // 
            this.patientID.HeaderText = "ID";
            this.patientID.Name = "patientID";
            this.patientID.ReadOnly = true;
            this.patientID.Width = 75;
            // 
            // patientName
            // 
            this.patientName.HeaderText = "Name";
            this.patientName.Name = "patientName";
            this.patientName.ReadOnly = true;
            this.patientName.Width = 220;
            // 
            // scheduleAppointmentButton
            // 
            this.scheduleAppointmentButton.Location = new System.Drawing.Point(871, 126);
            this.scheduleAppointmentButton.Name = "scheduleAppointmentButton";
            this.scheduleAppointmentButton.Size = new System.Drawing.Size(160, 33);
            this.scheduleAppointmentButton.TabIndex = 1;
            this.scheduleAppointmentButton.Text = "Schedule Appointment";
            this.scheduleAppointmentButton.UseVisualStyleBackColor = true;
            this.scheduleAppointmentButton.Click += new System.EventHandler(this.scheduleAppointmentButton_Click);
            // 
            // newPatientButton
            // 
            this.newPatientButton.Location = new System.Drawing.Point(871, 205);
            this.newPatientButton.Name = "newPatientButton";
            this.newPatientButton.Size = new System.Drawing.Size(160, 33);
            this.newPatientButton.TabIndex = 0;
            this.newPatientButton.Text = "New Patient";
            this.newPatientButton.UseVisualStyleBackColor = true;
            this.newPatientButton.Click += new System.EventHandler(this.newPatientButton_Click);
            // 
            // patientSearchGroupBox
            // 
            this.patientSearchGroupBox.Controls.Add(this.searchAllRadioButton);
            this.patientSearchGroupBox.Controls.Add(this.searchByDateRadioButton);
            this.patientSearchGroupBox.Controls.Add(this.searchByNameRadioButton);
            this.patientSearchGroupBox.Controls.Add(this.patientSearchButton);
            this.patientSearchGroupBox.Controls.Add(this.patientDOBDateTimePicker);
            this.patientSearchGroupBox.Controls.Add(this.patientFirstNameTextBox);
            this.patientSearchGroupBox.Controls.Add(this.dateOfBirthLabel);
            this.patientSearchGroupBox.Controls.Add(this.patientLastNameTextBox);
            this.patientSearchGroupBox.Location = new System.Drawing.Point(16, 0);
            this.patientSearchGroupBox.Name = "patientSearchGroupBox";
            this.patientSearchGroupBox.Size = new System.Drawing.Size(717, 81);
            this.patientSearchGroupBox.TabIndex = 10;
            this.patientSearchGroupBox.TabStop = false;
            this.patientSearchGroupBox.Text = "Patient Search";
            // 
            // searchAllRadioButton
            // 
            this.searchAllRadioButton.AutoSize = true;
            this.searchAllRadioButton.Checked = true;
            this.searchAllRadioButton.Location = new System.Drawing.Point(595, 56);
            this.searchAllRadioButton.Name = "searchAllRadioButton";
            this.searchAllRadioButton.Size = new System.Drawing.Size(86, 21);
            this.searchAllRadioButton.TabIndex = 13;
            this.searchAllRadioButton.TabStop = true;
            this.searchAllRadioButton.Text = "Search All";
            this.searchAllRadioButton.UseVisualStyleBackColor = true;
            this.searchAllRadioButton.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // searchByDateRadioButton
            // 
            this.searchByDateRadioButton.AutoSize = true;
            this.searchByDateRadioButton.Location = new System.Drawing.Point(595, 31);
            this.searchByDateRadioButton.Name = "searchByDateRadioButton";
            this.searchByDateRadioButton.Size = new System.Drawing.Size(118, 21);
            this.searchByDateRadioButton.TabIndex = 12;
            this.searchByDateRadioButton.Text = "Search by Date";
            this.searchByDateRadioButton.UseVisualStyleBackColor = true;
            this.searchByDateRadioButton.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // searchByNameRadioButton
            // 
            this.searchByNameRadioButton.AutoSize = true;
            this.searchByNameRadioButton.Location = new System.Drawing.Point(595, 6);
            this.searchByNameRadioButton.Name = "searchByNameRadioButton";
            this.searchByNameRadioButton.Size = new System.Drawing.Size(123, 21);
            this.searchByNameRadioButton.TabIndex = 11;
            this.searchByNameRadioButton.Text = "Search By Name";
            this.searchByNameRadioButton.UseVisualStyleBackColor = true;
            this.searchByNameRadioButton.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // patientSearchButton
            // 
            this.patientSearchButton.Location = new System.Drawing.Point(498, 30);
            this.patientSearchButton.Name = "patientSearchButton";
            this.patientSearchButton.Size = new System.Drawing.Size(75, 23);
            this.patientSearchButton.TabIndex = 4;
            this.patientSearchButton.Text = "Search";
            this.patientSearchButton.UseVisualStyleBackColor = true;
            this.patientSearchButton.Click += new System.EventHandler(this.patientSearchButton_Click);
            // 
            // patientDOBDateTimePicker
            // 
            this.patientDOBDateTimePicker.Checked = false;
            this.patientDOBDateTimePicker.Enabled = false;
            this.patientDOBDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.patientDOBDateTimePicker.Location = new System.Drawing.Point(363, 31);
            this.patientDOBDateTimePicker.MaxDate = new System.DateTime(2016, 11, 30, 0, 0, 0, 0);
            this.patientDOBDateTimePicker.Name = "patientDOBDateTimePicker";
            this.patientDOBDateTimePicker.Size = new System.Drawing.Size(112, 22);
            this.patientDOBDateTimePicker.TabIndex = 3;
            this.patientDOBDateTimePicker.Value = new System.DateTime(2016, 11, 29, 0, 0, 0, 0);
            // 
            // patientFirstNameTextBox
            // 
            this.patientFirstNameTextBox.Enabled = false;
            this.patientFirstNameTextBox.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.patientFirstNameTextBox.Location = new System.Drawing.Point(7, 31);
            this.patientFirstNameTextBox.Name = "patientFirstNameTextBox";
            this.patientFirstNameTextBox.Size = new System.Drawing.Size(112, 22);
            this.patientFirstNameTextBox.TabIndex = 1;
            this.patientFirstNameTextBox.Text = "First Name";
            this.patientFirstNameTextBox.Leave += new System.EventHandler(this.patientFirstNameTextBox_Leave);
            this.patientFirstNameTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.patientFirstNameTextBox_MouseDown);
            // 
            // dateOfBirthLabel
            // 
            this.dateOfBirthLabel.AutoSize = true;
            this.dateOfBirthLabel.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateOfBirthLabel.Location = new System.Drawing.Point(283, 34);
            this.dateOfBirthLabel.Name = "dateOfBirthLabel";
            this.dateOfBirthLabel.Size = new System.Drawing.Size(82, 17);
            this.dateOfBirthLabel.TabIndex = 8;
            this.dateOfBirthLabel.Text = "Date of Birth";
            // 
            // patientLastNameTextBox
            // 
            this.patientLastNameTextBox.Enabled = false;
            this.patientLastNameTextBox.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.patientLastNameTextBox.Location = new System.Drawing.Point(145, 31);
            this.patientLastNameTextBox.Name = "patientLastNameTextBox";
            this.patientLastNameTextBox.Size = new System.Drawing.Size(112, 22);
            this.patientLastNameTextBox.TabIndex = 2;
            this.patientLastNameTextBox.Text = "Last Name";
            this.patientLastNameTextBox.Leave += new System.EventHandler(this.patientLastNameTextBox_Leave);
            this.patientLastNameTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.patientLastNameTextBox_MouseDown);
            // 
            // adminTabPage
            // 
            this.adminTabPage.Controls.Add(this.clearButton);
            this.adminTabPage.Controls.Add(this.sqlResultsLabel);
            this.adminTabPage.Controls.Add(this.sqlTerminalSubmitButton);
            this.adminTabPage.Controls.Add(this.sqlTerminalTextLabel);
            this.adminTabPage.Controls.Add(this.sqlTerminalRichTextBox);
            this.adminTabPage.Controls.Add(this.sqlResultsDataView);
            this.adminTabPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adminTabPage.Location = new System.Drawing.Point(4, 22);
            this.adminTabPage.Name = "adminTabPage";
            this.adminTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.adminTabPage.Size = new System.Drawing.Size(1098, 514);
            this.adminTabPage.TabIndex = 1;
            this.adminTabPage.Text = "Administrator";
            this.adminTabPage.UseVisualStyleBackColor = true;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(265, 459);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(160, 33);
            this.clearButton.TabIndex = 16;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // sqlResultsLabel
            // 
            this.sqlResultsLabel.AutoSize = true;
            this.sqlResultsLabel.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sqlResultsLabel.Location = new System.Drawing.Point(472, 14);
            this.sqlResultsLabel.Name = "sqlResultsLabel";
            this.sqlResultsLabel.Size = new System.Drawing.Size(70, 16);
            this.sqlResultsLabel.TabIndex = 15;
            this.sqlResultsLabel.Text = "SQL Results";
            // 
            // sqlTerminalSubmitButton
            // 
            this.sqlTerminalSubmitButton.Location = new System.Drawing.Point(19, 459);
            this.sqlTerminalSubmitButton.Name = "sqlTerminalSubmitButton";
            this.sqlTerminalSubmitButton.Size = new System.Drawing.Size(160, 33);
            this.sqlTerminalSubmitButton.TabIndex = 14;
            this.sqlTerminalSubmitButton.Text = "Submit";
            this.sqlTerminalSubmitButton.UseVisualStyleBackColor = true;
            this.sqlTerminalSubmitButton.Click += new System.EventHandler(this.sqlTerminalSubmitButton_Click);
            // 
            // sqlTerminalTextLabel
            // 
            this.sqlTerminalTextLabel.AutoSize = true;
            this.sqlTerminalTextLabel.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sqlTerminalTextLabel.Location = new System.Drawing.Point(16, 14);
            this.sqlTerminalTextLabel.Name = "sqlTerminalTextLabel";
            this.sqlTerminalTextLabel.Size = new System.Drawing.Size(82, 16);
            this.sqlTerminalTextLabel.TabIndex = 13;
            this.sqlTerminalTextLabel.Text = "SQL Terminal";
            // 
            // sqlTerminalRichTextBox
            // 
            this.sqlTerminalRichTextBox.Location = new System.Drawing.Point(19, 39);
            this.sqlTerminalRichTextBox.Name = "sqlTerminalRichTextBox";
            this.sqlTerminalRichTextBox.Size = new System.Drawing.Size(407, 400);
            this.sqlTerminalRichTextBox.TabIndex = 12;
            this.sqlTerminalRichTextBox.Text = "";
            // 
            // sqlResultsDataView
            // 
            this.sqlResultsDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sqlResultsDataView.Location = new System.Drawing.Point(475, 39);
            this.sqlResultsDataView.Name = "sqlResultsDataView";
            this.sqlResultsDataView.Size = new System.Drawing.Size(603, 400);
            this.sqlResultsDataView.TabIndex = 0;
            // 
            // logoutButton
            // 
            this.logoutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logoutButton.Location = new System.Drawing.Point(529, 27);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(84, 35);
            this.logoutButton.TabIndex = 4;
            this.logoutButton.Text = "Logout";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(1034, 27);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(84, 35);
            this.exitButton.TabIndex = 6;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // HealthCareSystemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1137, 645);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.employeeTabControl);
            this.Controls.Add(this.employeeInfoGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HealthCareSystemForm";
            this.Text = "Healthcare System";
            this.employeeInfoGroupBox.ResumeLayout(false);
            this.employeeInfoGroupBox.PerformLayout();
            this.employeeTabControl.ResumeLayout(false);
            this.nurseTabPage.ResumeLayout(false);
            this.patientInfoGroupBox.ResumeLayout(false);
            this.patientInfoGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientAppointmentDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientDataGridView)).EndInit();
            this.patientSearchGroupBox.ResumeLayout(false);
            this.patientSearchGroupBox.PerformLayout();
            this.adminTabPage.ResumeLayout(false);
            this.adminTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sqlResultsDataView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.GroupBox employeeInfoGroupBox;
        private System.Windows.Forms.Label employeeIDLabel;
        private System.Windows.Forms.Label employeeNameLabel;
        private System.Windows.Forms.TabControl employeeTabControl;
        private System.Windows.Forms.TabPage nurseTabPage;
        private System.Windows.Forms.TabPage adminTabPage;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Button scheduleAppointmentButton;
        private System.Windows.Forms.Button newPatientButton;
        private System.Windows.Forms.DataGridView patientDataGridView;
        private System.Windows.Forms.GroupBox patientSearchGroupBox;
        private System.Windows.Forms.DateTimePicker patientDOBDateTimePicker;
        private System.Windows.Forms.TextBox patientFirstNameTextBox;
        private System.Windows.Forms.Label dateOfBirthLabel;
        private System.Windows.Forms.TextBox patientLastNameTextBox;
        private System.Windows.Forms.Button patientSearchButton;
        private System.Windows.Forms.DataGridView sqlResultsDataView;
        private System.Windows.Forms.RadioButton searchByDateRadioButton;
        private System.Windows.Forms.RadioButton searchByNameRadioButton;
        private System.Windows.Forms.RichTextBox sqlTerminalRichTextBox;
        private System.Windows.Forms.Label sqlTerminalTextLabel;
        private System.Windows.Forms.GroupBox patientInfoGroupBox;
        private System.Windows.Forms.Label appointmentLabel;
        private System.Windows.Forms.DataGridView patientAppointmentDataGridView;
        private System.Windows.Forms.Button sqlTerminalSubmitButton;
        private System.Windows.Forms.Label patientGenderField;
        private System.Windows.Forms.Label patientPhoneNumberField;
        private System.Windows.Forms.Label patientAddressField;
        private System.Windows.Forms.Label patientDOBField;
        private System.Windows.Forms.Label patientIDField;
        private System.Windows.Forms.Label patientNameField;
        private System.Windows.Forms.Label patientGenderLabel;
        private System.Windows.Forms.Label patientPhoneNumberLabel;
        private System.Windows.Forms.Label patientAddressLabel;
        private System.Windows.Forms.Label patientDOBLabel;
        private System.Windows.Forms.Label patientIDLabel;
        private System.Windows.Forms.Label patientNameLabel;
        private System.Windows.Forms.RadioButton searchAllRadioButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn patientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn patientName;
        private System.Windows.Forms.Label IDLabel;
        private System.Windows.Forms.Label sqlResultsLabel;
        private System.Windows.Forms.Button viewAppointmentButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn appointmentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn appointmentDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn appointmentTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn appointmentReason;
        private System.Windows.Forms.Button clearButton;
    }
}