﻿namespace HealthCareSystem
{
    partial class NewPatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewPatientForm));
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.midinitLabel = new System.Windows.Forms.Label();
            this.lastNameLabel = new System.Windows.Forms.Label();
            this.addressLabel = new System.Windows.Forms.Label();
            this.phoneNumberLabel = new System.Windows.Forms.Label();
            this.genderLabel = new System.Windows.Forms.Label();
            this.firstNameField = new System.Windows.Forms.TextBox();
            this.phoneNumberField = new System.Windows.Forms.TextBox();
            this.addressField = new System.Windows.Forms.TextBox();
            this.lastNameField = new System.Windows.Forms.TextBox();
            this.midinitField = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.cityLabel = new System.Windows.Forms.Label();
            this.zipCodeLabel = new System.Windows.Forms.Label();
            this.stateLabel = new System.Windows.Forms.Label();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.zipCodeTextBox = new System.Windows.Forms.TextBox();
            this.addPatientLabel = new System.Windows.Forms.Label();
            this.genderComboBox = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.error1 = new System.Windows.Forms.Label();
            this.error4 = new System.Windows.Forms.Label();
            this.error5 = new System.Windows.Forms.Label();
            this.error3 = new System.Windows.Forms.Label();
            this.error7 = new System.Windows.Forms.Label();
            this.error6 = new System.Windows.Forms.Label();
            this.error2 = new System.Windows.Forms.Label();
            this.dobField = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstNameLabel.Location = new System.Drawing.Point(66, 71);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(73, 16);
            this.firstNameLabel.TabIndex = 1;
            this.firstNameLabel.Text = "First Name";
            // 
            // midinitLabel
            // 
            this.midinitLabel.AutoSize = true;
            this.midinitLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.midinitLabel.Location = new System.Drawing.Point(57, 118);
            this.midinitLabel.Name = "midinitLabel";
            this.midinitLabel.Size = new System.Drawing.Size(82, 16);
            this.midinitLabel.TabIndex = 2;
            this.midinitLabel.Text = "Middle Initial";
            // 
            // lastNameLabel
            // 
            this.lastNameLabel.AutoSize = true;
            this.lastNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastNameLabel.Location = new System.Drawing.Point(66, 165);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Size = new System.Drawing.Size(73, 16);
            this.lastNameLabel.TabIndex = 3;
            this.lastNameLabel.Text = "Last Name";
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addressLabel.Location = new System.Drawing.Point(80, 212);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(59, 16);
            this.addressLabel.TabIndex = 4;
            this.addressLabel.Text = "Address";
            // 
            // phoneNumberLabel
            // 
            this.phoneNumberLabel.AutoSize = true;
            this.phoneNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phoneNumberLabel.Location = new System.Drawing.Point(41, 400);
            this.phoneNumberLabel.Name = "phoneNumberLabel";
            this.phoneNumberLabel.Size = new System.Drawing.Size(98, 16);
            this.phoneNumberLabel.TabIndex = 5;
            this.phoneNumberLabel.Text = "Phone Number";
            // 
            // genderLabel
            // 
            this.genderLabel.AutoSize = true;
            this.genderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genderLabel.Location = new System.Drawing.Point(86, 447);
            this.genderLabel.Name = "genderLabel";
            this.genderLabel.Size = new System.Drawing.Size(53, 16);
            this.genderLabel.TabIndex = 6;
            this.genderLabel.Text = "Gender";
            // 
            // firstNameField
            // 
            this.firstNameField.Location = new System.Drawing.Point(178, 68);
            this.firstNameField.Name = "firstNameField";
            this.firstNameField.Size = new System.Drawing.Size(138, 20);
            this.firstNameField.TabIndex = 1;
            // 
            // phoneNumberField
            // 
            this.phoneNumberField.Location = new System.Drawing.Point(178, 397);
            this.phoneNumberField.Name = "phoneNumberField";
            this.phoneNumberField.Size = new System.Drawing.Size(138, 20);
            this.phoneNumberField.TabIndex = 8;
            // 
            // addressField
            // 
            this.addressField.Location = new System.Drawing.Point(178, 209);
            this.addressField.Multiline = true;
            this.addressField.Name = "addressField";
            this.addressField.Size = new System.Drawing.Size(138, 20);
            this.addressField.TabIndex = 4;
            // 
            // lastNameField
            // 
            this.lastNameField.Location = new System.Drawing.Point(178, 162);
            this.lastNameField.Name = "lastNameField";
            this.lastNameField.Size = new System.Drawing.Size(138, 20);
            this.lastNameField.TabIndex = 3;
            // 
            // midinitField
            // 
            this.midinitField.Location = new System.Drawing.Point(178, 115);
            this.midinitField.Name = "midinitField";
            this.midinitField.Size = new System.Drawing.Size(138, 20);
            this.midinitField.TabIndex = 2;
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(150, 547);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(104, 32);
            this.submitButton.TabIndex = 10;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // cityLabel
            // 
            this.cityLabel.AutoSize = true;
            this.cityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityLabel.Location = new System.Drawing.Point(109, 259);
            this.cityLabel.Name = "cityLabel";
            this.cityLabel.Size = new System.Drawing.Size(30, 16);
            this.cityLabel.TabIndex = 16;
            this.cityLabel.Text = "City";
            // 
            // zipCodeLabel
            // 
            this.zipCodeLabel.AutoSize = true;
            this.zipCodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zipCodeLabel.Location = new System.Drawing.Point(76, 353);
            this.zipCodeLabel.Name = "zipCodeLabel";
            this.zipCodeLabel.Size = new System.Drawing.Size(63, 16);
            this.zipCodeLabel.TabIndex = 17;
            this.zipCodeLabel.Text = "Zip Code";
            // 
            // stateLabel
            // 
            this.stateLabel.AutoSize = true;
            this.stateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stateLabel.Location = new System.Drawing.Point(100, 306);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(39, 16);
            this.stateLabel.TabIndex = 18;
            this.stateLabel.Text = "State";
            // 
            // cityTextBox
            // 
            this.cityTextBox.Location = new System.Drawing.Point(178, 256);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(138, 20);
            this.cityTextBox.TabIndex = 5;
            // 
            // zipCodeTextBox
            // 
            this.zipCodeTextBox.Location = new System.Drawing.Point(178, 350);
            this.zipCodeTextBox.Name = "zipCodeTextBox";
            this.zipCodeTextBox.Size = new System.Drawing.Size(138, 20);
            this.zipCodeTextBox.TabIndex = 7;
            // 
            // addPatientLabel
            // 
            this.addPatientLabel.AutoSize = true;
            this.addPatientLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addPatientLabel.Location = new System.Drawing.Point(107, 22);
            this.addPatientLabel.Name = "addPatientLabel";
            this.addPatientLabel.Size = new System.Drawing.Size(202, 20);
            this.addPatientLabel.TabIndex = 0;
            this.addPatientLabel.Text = "New Patient Information";
            // 
            // genderComboBox
            // 
            this.genderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.genderComboBox.FormattingEnabled = true;
            this.genderComboBox.Items.AddRange(new object[] {
            "Female",
            "Male"});
            this.genderComboBox.Location = new System.Drawing.Point(178, 447);
            this.genderComboBox.Name = "genderComboBox";
            this.genderComboBox.Size = new System.Drawing.Size(138, 21);
            this.genderComboBox.TabIndex = 9;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Alabama ",
            "Alaska ",
            "Arizona ",
            "Arkansas ",
            "California ",
            "Colorado ",
            "Connecticut ",
            "Delaware ",
            "Florida ",
            "Georgia ",
            "Hawaii ",
            "Idaho ",
            "Illinois Indiana ",
            "Iowa ",
            "Kansas ",
            "Kentucky ",
            "Louisiana ",
            "Maine ",
            "Maryland ",
            "Massachusetts ",
            "Michigan ",
            "Minnesota ",
            "Mississippi ",
            "Missouri ",
            "Montana Nebraska ",
            "Nevada ",
            "New Hampshire ",
            "New Jersey ",
            "New Mexico ",
            "New York ",
            "North Carolina ",
            "North Dakota ",
            "Ohio ",
            "Oklahoma ",
            "Oregon ",
            "Pennsylvania Rhode Island ",
            "South Carolina ",
            "South Dakota ",
            "Tennessee ",
            "Texas ",
            "Utah ",
            "Vermont ",
            "Virginia ",
            "Washington ",
            "West Virginia ",
            "Wisconsin ",
            "Wyoming"});
            this.comboBox1.Location = new System.Drawing.Point(178, 306);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(138, 21);
            this.comboBox1.TabIndex = 6;
            // 
            // error1
            // 
            this.error1.AutoSize = true;
            this.error1.ForeColor = System.Drawing.Color.Red;
            this.error1.Location = new System.Drawing.Point(333, 71);
            this.error1.Name = "error1";
            this.error1.Size = new System.Drawing.Size(64, 13);
            this.error1.TabIndex = 19;
            this.error1.Text = "Invalid input";
            this.error1.Visible = false;
            // 
            // error4
            // 
            this.error4.AutoSize = true;
            this.error4.ForeColor = System.Drawing.Color.Red;
            this.error4.Location = new System.Drawing.Point(333, 212);
            this.error4.Name = "error4";
            this.error4.Size = new System.Drawing.Size(64, 13);
            this.error4.TabIndex = 28;
            this.error4.Text = "Invalid input";
            this.error4.Visible = false;
            // 
            // error5
            // 
            this.error5.AutoSize = true;
            this.error5.ForeColor = System.Drawing.Color.Red;
            this.error5.Location = new System.Drawing.Point(333, 261);
            this.error5.Name = "error5";
            this.error5.Size = new System.Drawing.Size(64, 13);
            this.error5.TabIndex = 29;
            this.error5.Text = "Invalid input";
            this.error5.Visible = false;
            // 
            // error3
            // 
            this.error3.AutoSize = true;
            this.error3.ForeColor = System.Drawing.Color.Red;
            this.error3.Location = new System.Drawing.Point(333, 165);
            this.error3.Name = "error3";
            this.error3.Size = new System.Drawing.Size(64, 13);
            this.error3.TabIndex = 30;
            this.error3.Text = "Invalid input";
            this.error3.Visible = false;
            // 
            // error7
            // 
            this.error7.AutoSize = true;
            this.error7.ForeColor = System.Drawing.Color.Red;
            this.error7.Location = new System.Drawing.Point(333, 402);
            this.error7.Name = "error7";
            this.error7.Size = new System.Drawing.Size(64, 13);
            this.error7.TabIndex = 31;
            this.error7.Text = "Invalid input";
            this.error7.Visible = false;
            // 
            // error6
            // 
            this.error6.AutoSize = true;
            this.error6.ForeColor = System.Drawing.Color.Red;
            this.error6.Location = new System.Drawing.Point(333, 353);
            this.error6.Name = "error6";
            this.error6.Size = new System.Drawing.Size(64, 13);
            this.error6.TabIndex = 32;
            this.error6.Text = "Invalid input";
            this.error6.Visible = false;
            // 
            // error2
            // 
            this.error2.AutoSize = true;
            this.error2.ForeColor = System.Drawing.Color.Red;
            this.error2.Location = new System.Drawing.Point(333, 118);
            this.error2.Name = "error2";
            this.error2.Size = new System.Drawing.Size(64, 13);
            this.error2.TabIndex = 33;
            this.error2.Text = "Invalid input";
            this.error2.Visible = false;
            // 
            // dobField
            // 
            this.dobField.Checked = false;
            this.dobField.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dobField.Location = new System.Drawing.Point(178, 497);
            this.dobField.MaxDate = new System.DateTime(2016, 11, 30, 0, 0, 0, 0);
            this.dobField.Name = "dobField";
            this.dobField.Size = new System.Drawing.Size(138, 20);
            this.dobField.TabIndex = 34;
            this.dobField.Value = new System.DateTime(2016, 11, 30, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 497);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Birth Date";
            // 
            // NewPatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 591);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dobField);
            this.Controls.Add(this.error2);
            this.Controls.Add(this.error6);
            this.Controls.Add(this.error7);
            this.Controls.Add(this.error3);
            this.Controls.Add(this.error5);
            this.Controls.Add(this.error4);
            this.Controls.Add(this.error1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.genderComboBox);
            this.Controls.Add(this.zipCodeTextBox);
            this.Controls.Add(this.cityTextBox);
            this.Controls.Add(this.stateLabel);
            this.Controls.Add(this.zipCodeLabel);
            this.Controls.Add(this.cityLabel);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.midinitField);
            this.Controls.Add(this.lastNameField);
            this.Controls.Add(this.addressField);
            this.Controls.Add(this.phoneNumberField);
            this.Controls.Add(this.firstNameField);
            this.Controls.Add(this.genderLabel);
            this.Controls.Add(this.phoneNumberLabel);
            this.Controls.Add(this.addressLabel);
            this.Controls.Add(this.lastNameLabel);
            this.Controls.Add(this.midinitLabel);
            this.Controls.Add(this.firstNameLabel);
            this.Controls.Add(this.addPatientLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewPatientForm";
            this.Text = "New Patient";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.Label midinitLabel;
        private System.Windows.Forms.Label lastNameLabel;
        private System.Windows.Forms.Label addressLabel;
        private System.Windows.Forms.Label phoneNumberLabel;
        private System.Windows.Forms.Label genderLabel;
        private System.Windows.Forms.TextBox firstNameField;
        private System.Windows.Forms.TextBox phoneNumberField;
        private System.Windows.Forms.TextBox addressField;
        private System.Windows.Forms.TextBox lastNameField;
        private System.Windows.Forms.TextBox midinitField;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label cityLabel;
        private System.Windows.Forms.Label zipCodeLabel;
        private System.Windows.Forms.Label stateLabel;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.TextBox zipCodeTextBox;
        private System.Windows.Forms.Label addPatientLabel;
        private System.Windows.Forms.ComboBox genderComboBox;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label error1;
        private System.Windows.Forms.Label error4;
        private System.Windows.Forms.Label error5;
        private System.Windows.Forms.Label error3;
        private System.Windows.Forms.Label error7;
        private System.Windows.Forms.Label error6;
        private System.Windows.Forms.Label error2;
        private System.Windows.Forms.DateTimePicker dobField;
        private System.Windows.Forms.Label label1;
    }
}