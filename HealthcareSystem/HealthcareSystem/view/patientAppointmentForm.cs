﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HealthcareSystem.controller;
using HealthcareSystem.model;
using HealthCareSystem.controller;
using HealthCareSystem.model;

namespace HealthcareSystem.view
{
    public partial class PatientAppointmentForm : Form
    {
        private readonly Patient currentPatient;
        private readonly Appointment currentAppointment;
        private readonly Employee currentDoctor;
        private readonly Employee currentNurse;
        private RoutineCheck currentCheck;
        private Diagnosis currentDiagnosis;

        private DiagnosisController diagnosisController;
        private RoutineChecksController routineCheckController;

        public PatientAppointmentForm()
        {
            InitializeComponent();
        }

        public PatientAppointmentForm(int PatientID, int appointmentID, int nurseID)
        {
            InitializeComponent();
            this.currentPatient = AccountRetrieval.GetPatientInfo(PatientID);
            AppointmentController appointmentController = new AppointmentController();
            this.currentAppointment = appointmentController.GetAppointmentById(appointmentID);
            this.currentDoctor = AccountRetrieval.GetEmployeeInfo(this.currentAppointment.DoctorID);
            this.currentNurse = AccountRetrieval.GetEmployeeInfo(nurseID);
            this.RefreshRoutineCheckInformation();

            this.appointmentIDFeild.Text = "" + this.currentAppointment.Id;
            this.appointmentDateFeild.Text = "" + this.currentAppointment.Date;
            this.doctorNameFeild.Text = "Dr." + this.currentDoctor.FirstName + " " + this.currentDoctor.Midinit + ". " +
                                        this.currentDoctor.LastName;
            this.reasonForVisitFeild.Text = this.currentAppointment.Reason;
            this.patientIDFeild.Text = this.currentPatient.FirstName + " " + this.currentPatient.Midinit + ". " +
                                       this.currentPatient.LastName;

            this.PopulateTestToOrder();
            this.orderTestButton.Enabled = false;
            this.diagnosisController = new DiagnosisController();
            this.RefreshDiagnosisInformation();
            this.RefreshRoutineCheckInformation();
            this.routineCheckController = new RoutineChecksController();
        }

        private void PopulateTestToOrder()
        {
            TestController controller = new TestController();
            foreach (var test in controller.GetAllTests())
            {
                this.orderedTestDataGridView.Rows.Add(false, test.Name);
            }
        }

        private void recordDataButton_Click(object sender, EventArgs e)
        {
            RoutineChecksForm form = new RoutineChecksForm(this.currentPatient.Id, this.currentNurse.Id,
                this.currentAppointment.Id, this);
            form.Show();
        }

        public void RefreshRoutineCheckInformation()
        {
            try
            {
                RoutineChecksController controller = new RoutineChecksController();
                this.currentCheck = controller.GetByAppointmentId(this.currentAppointment.Id);

                this.bloodPressureField.Text = this.currentCheck.BloodPressure;
                this.bodyTemperatureField.Text = this.currentCheck.BodyTemp.ToString();
                this.pulseField.Text = this.currentCheck.PulseBPM.ToString();
                this.systolicReadingField.Text = this.currentCheck.SystolicReading.ToString();
                this.diastolicReadingField.Text = this.currentCheck.DiastolicReading.ToString();
                this.recordDataButton.Enabled = false;
                this.PopulateListOfSymptoms();

                this.initialDiagnosisTextBox.Enabled = true;
                this.submitInitialDiagnosisButton.Enabled = true;
                this.RefreshDiagnosisInformation();

            }
            catch (Exception e)
            {
                this.recordDataButton.Enabled = true;
                this.initialDiagnosisTextBox.Enabled = false;
                this.submitInitialDiagnosisButton.Enabled = false;

            }

        }

        private bool CheckIfRoutineChecksExist()
        {
            bool exist;
            RoutineChecksController controller = new RoutineChecksController();
            try
            {
                this.currentCheck = controller.GetByAppointmentId(this.currentAppointment.Id);
                exist = true;
            }
            catch (Exception e)
            {
                exist = false;
            }

            return exist;
        }

        private void RefreshDiagnosisInformation()
        {
            this.currentDiagnosis = null;
            DiagnosisController controller = new DiagnosisController();
            if (this.CheckIfInitialDiagnosisExist() && this.CheckIfFinalDiagnosisExist())
            {
                this.disableEverything();
            }
            else if (!this.CheckIfInitialDiagnosisExist() && !this.CheckIfRoutineChecksExist())
            {
                this.finalDiagnosisTextBox.Enabled = false;
                this.finalDiagnosisSubmitButton.Enabled = false;

                this.initialDiagnosisTextBox.Enabled = false;
                this.submitInitialDiagnosisButton.Enabled = false;

                this.orderedTestDataGridView.Enabled = false;
                this.orderTestButton.Enabled = false;

                this.testResultsDataGridView.Enabled = false;

            }
            else if (!this.CheckIfInitialDiagnosisExist() && this.CheckIfRoutineChecksExist())
            {
                this.finalDiagnosisTextBox.Enabled = false;
                this.finalDiagnosisSubmitButton.Enabled = false;

                this.initialDiagnosisTextBox.Enabled = true;
                this.submitInitialDiagnosisButton.Enabled = true;

                this.orderedTestDataGridView.Enabled = false;
                this.orderTestButton.Enabled = false;

                this.testResultsDataGridView.Enabled = false;
            }
            else if (this.CheckIfInitialDiagnosisExist())
            {
                this.finalDiagnosisTextBox.Enabled = true;
                this.finalDiagnosisSubmitButton.Enabled = true;
                this.initialDiagnosisTextBox.Enabled = false;
                this.submitInitialDiagnosisButton.Enabled = false;

                this.orderedTestDataGridView.Enabled = true;
                this.orderTestButton.Enabled = true;

                this.testResultsDataGridView.Enabled = true;
                currentDiagnosis = controller.GetAllDiagnosisesByAppointmentID(this.currentAppointment.Id);
                this.initialDiagnosisTextBox.Text = currentDiagnosis.InitialDiagnosis;
            }
        }

        private void disableEverything()
        {
            DiagnosisController controller = new DiagnosisController();
            this.orderedTestDataGridView.Enabled = false;
            this.testResultsDataGridView.Enabled = false;
            this.orderTestButton.Enabled = false;

            this.finalDiagnosisTextBox.Enabled = false;
            this.initialDiagnosisTextBox.Enabled = false;

            this.submitInitialDiagnosisButton.Enabled = false;
            this.finalDiagnosisSubmitButton.Enabled = false;

            this.testResultsDataGridView.Enabled = false;

            this.orderedTestDataGridView.Enabled = false;
            this.orderTestButton.Enabled = false;

            currentDiagnosis = controller.GetAllDiagnosisesByAppointmentID(this.currentAppointment.Id);
            this.initialDiagnosisTextBox.Text = currentDiagnosis.InitialDiagnosis;
            this.finalDiagnosisTextBox.Text = currentDiagnosis.FinalDiagnosis;
        }

        private void PopulateListOfSymptoms()
        {
            HasSymptomsController controller = new HasSymptomsController();

            foreach (var symptom in controller.GetAllSymptomsByID(this.currentAppointment.Id))
            {
                this.patientSymptomsDataGridView.Rows.Add(symptom.Name);
            }
        }

        private void orderedTestDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            bool isChecked = Convert.ToBoolean(this.orderedTestDataGridView.SelectedRows[0].Cells[0].Value);

            if (!isChecked)
            {
                this.orderedTestDataGridView.SelectedRows[0].Cells[0].Value = true;
            }
            else
            {
                this.orderedTestDataGridView.SelectedRows[0].Cells[0].Value = false;

            }

            if (this.CheckIfOneTestIsSelected())
            {
                this.orderTestButton.Enabled = true;
            }
            else
            {
                this.orderTestButton.Enabled = false;
            }

        }

        private bool CheckIfOneTestIsSelected()
        {
            bool atLeastOneSelected = false;
            foreach (DataGridViewRow test in this.orderedTestDataGridView.Rows)
            {
                if (Convert.ToBoolean(test.Cells[0].Value))
                {
                    atLeastOneSelected = true;
                }
            }

            return atLeastOneSelected;
        }

        private void submitInitialDiagnosisButton_Click(object sender, EventArgs e)
        {
            Diagnosis diagnosis = new Diagnosis();
            diagnosis.AppointmentID = this.currentAppointment.Id;
            diagnosis.InitialDiagnosis = this.initialDiagnosisTextBox.Text;
            diagnosis.FinalDiagnosis = null;
            this.diagnosisController.Add(diagnosis);
            this.RefreshDiagnosisInformation();
 
            MessageBox.Show("Initial diagnosis has been submitted");

        }

        private bool CheckIfInitialDiagnosisExist()
        {
            bool exist = false;
            DiagnosisController controller = new DiagnosisController();
            try
            {
                exist = !controller.isInitialDiagnosisNull(this.currentAppointment.Id);
            }
            catch
            {
                return false;
            }
            return exist;
        }

        private bool CheckIfFinalDiagnosisExist()
        {
            DiagnosisController controller = new DiagnosisController();
            bool exist = !controller.isFinalDiagnosisNull(this.currentAppointment.Id);

            return exist;
        }

        private void finalDiagnosisSubmitButton_Click(object sender, EventArgs e)
        {
            

            var tempDiagnosis = this.diagnosisController.GetAllDiagnosisesByAppointmentID(this.currentAppointment.Id);
            tempDiagnosis.FinalDiagnosis = this.finalDiagnosisTextBox.Text;
            this.diagnosisController.FinalizeDiagnosis(tempDiagnosis);
            this.RefreshDiagnosisInformation();
            this.disableEverything();
            MessageBox.Show("Final diagnosis has be submited");
            
        }

        private void orderTestButton_Click(object sender, EventArgs e)
        {

            TestsOrderedController controller = new TestsOrderedController();
            foreach (TestOrdered selectedTest in this.getSelectedTest())
            {

                TestOrdered test = new TestOrdered();
                test.Name = selectedTest.Name;
                test.DiagnosisID = this.currentDiagnosis.Id;
                controller.AddOrderTest(test);
                this.testResultsDataGridView.Rows.Add(test.Name, "No Data entered", "No Data Entered");
            }

            MessageBox.Show("Test have been ordered");
        }

        private IList<TestOrdered> getSelectedTest()
        {
            IList<TestOrdered> selectedTest = new List<TestOrdered>();

            foreach (DataGridViewRow test in this.orderedTestDataGridView.Rows)
            {
                if (Convert.ToBoolean(test.Cells[0].Value))
                {
                    TestOrdered tempTest = new TestOrdered();
                    tempTest.Name = test.Cells[1].Value.ToString();
                    selectedTest.Add(tempTest);
                    test.ReadOnly = true;
                }
            }

            return selectedTest;
        }

    }
}

