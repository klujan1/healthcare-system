﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSystem.model
{
    /// <summary>
    /// Test order that can be made.
    /// </summary>
    class TestOrdered
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the diagnosis identifier.
        /// </summary>
        /// <value>
        /// The diagnosis identifier.
        /// </value>
        public int DiagnosisID { get; set; }
    }
}
