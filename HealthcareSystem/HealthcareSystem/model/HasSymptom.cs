﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSystem.model
{
    /// <summary>
    /// Has symptom.
    /// </summary>
    class HasSymptom
    {
        /// <summary>
        /// Gets or sets the appointment identifier.
        /// </summary>
        /// <value>
        /// The appointment identifier.
        /// </value>
        public int AppointmentID { get; set; }

        /// <summary>
        /// Gets or sets the name of the symptom.
        /// </summary>
        /// <value>
        /// The name of the symptom.
        /// </value>
        public string SymptomName { get; set; }
    }
}
