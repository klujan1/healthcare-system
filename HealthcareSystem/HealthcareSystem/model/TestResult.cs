﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSystem.model
{
    /// <summary>
    /// TEst result.
    /// </summary>
    public class TestResult
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the test.
        /// </summary>
        /// <value>
        /// The name of the test.
        /// </value>
        public string TestName { get; set; }

        /// <summary>
        /// Gets or sets the diagnosis identifier.
        /// </summary>
        /// <value>
        /// The diagnosis identifier.
        /// </value>
        public int DiagnosisID { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public string Result { get; set; }

        /// <summary>
        /// Gets or sets the date performed.
        /// </summary>
        /// <value>
        /// The date performed.
        /// </value>
        public DateTime DatePerformed { get; set; }
    }
}
