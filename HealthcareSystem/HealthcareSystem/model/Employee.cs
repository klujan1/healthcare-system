﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSystem.model
{
    public class Employee
    {
        /// <summary>
        /// The title
        /// </summary>
        private string title;

        public Employee()
        {
            //this.title = title;
        }
        /*
         * Gets or sets the Id number.
         */
        public int Id { get; set; }

        /*
         * Gets for sets the first name.
         */
        public string FirstName { get; set; }

        /*
         * Gets or sets the middle initial.
         */
        public string Midinit { get; set; }

        /*
         * Gets or sets the last name.
         */
        public string LastName { get; set; }

        /*
         * Gets or sets the birth date.
         */
        public DateTime DateOfBirth { get; set; }

        /*
         * Gets or sets the address.
         */
        public string Street { get; set; }
        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public string State { get; set; }
        /// <summary>
        /// Gets or sets the zipcode.
        /// </summary>
        /// <value>
        /// The zipcode.
        /// </value>
        public string Zipcode { get; set; }

        /*
         * Gets or sets phone number.
         */
        public string PhoneNumber { get; set; }

        /*
         * Gets or sets the title of the employee.
         */
        public string Title { get; set; }
    }
}
