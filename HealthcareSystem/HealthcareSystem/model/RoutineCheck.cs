﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSystem.model
{
    /// <summary>
    /// Routine check for a patient.
    /// </summary>
    class RoutineCheck
    {
        /// <summary>
        /// Gets or sets the appointment identifier.
        /// </summary>
        /// <value>
        /// The appointment identifier.
        /// </value>
        public int AppointmentID { get; set; }

        /// <summary>
        /// Gets or sets the blood pressure.
        /// </summary>
        /// <value>
        /// The blood pressure.
        /// </value>
        public string BloodPressure { get; set; }

        /// <summary>
        /// Gets or sets the body temporary.
        /// </summary>
        /// <value>
        /// The body temporary.
        /// </value>
        public double BodyTemp { get; set; }

        /// <summary>
        /// Gets or sets the pulse BPM.
        /// </summary>
        /// <value>
        /// The pulse BPM.
        /// </value>
        public int PulseBPM { get; set; }

        /// <summary>
        /// Gets or sets the systolic reading.
        /// </summary>
        /// <value>
        /// The systolic reading.
        /// </value>
        public int SystolicReading { get; set; }

        /// <summary>
        /// Gets or sets the diastolic reading.
        /// </summary>
        /// <value>
        /// The diastolic reading.
        /// </value>
        public int DiastolicReading { get; set; }

        /// <summary>
        /// Gets or sets the nurse identifier.
        /// </summary>
        /// <value>
        /// The nurse identifier.
        /// </value>
        public int NurseID { get; set; }

        /// <summary>
        /// Sets the blood pressure.
        /// </summary>
        /// <exception cref="Exception">Systolic Reading and Diastolic Reading must be set first.</exception>
        public void SetBloodPressure()
        {
            if (SystolicReading == 0 || DiastolicReading == 0)
            {
                throw new Exception("Systolic Reading and Diastolic Reading must be set first.");
            }
            BloodPressure = SystolicReading + " / " + DiastolicReading;
        }
    }
}
