﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSystem.model
{
    /*
     * An employee's account that contains their Id and a password.
     */
    public class Account
    {
        /*
         * Gets or sets the Id.
         */
        public int Id { get; set; }

        /*
         * Gets or sets the password.
         */
        public string Password { get; set; }

    }
}
