﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSystem.model
{
    public class Patient
    {
        /*
         * Gets or sets the Id.
         */ 
        public int Id { get; set; }

        /*
         * Gets or sets the first name.
         */
        public string FirstName { get; set; }

        /*
         * Gets or sets the middle initial.
         */
        public string Midinit { get; set; }

        /*
         * Gets or sets the last name.
         */
        public string LastName { get; set; }

        /*
         * Gets or sets the birth date.
         */
        public DateTime DateOfBirth { get; set; }

        /*
         * Gets or sets the street
         */
        public string Street { get; set; }
        /*
         * Gets or sets the city
         */
        public string City { get; set; }
        /*
         * Gets or sets the state
         */
        public string State { get; set; }
        /*
         * Gets or sets the zipcode
         */
        public string Zipcode { get; set; }

        /*
         * Gets or sets the phone number.
         */
        public string PhoneNumber { get; set; }

        /*
         * Gets or sets the gender.
         */
        public string Gender { get; set; }
    }
}
