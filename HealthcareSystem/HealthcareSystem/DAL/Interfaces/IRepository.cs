﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSystem.DAL.Interfaces
{
    /*
     * Interface for the repositories.
     */
    interface IRepository<T>
    {
        /*
         * Adds the param to the database.
         * 
         * @param entity the object that is to be added
         */
        void Add(T entity);

        /*
         * Gets the object from the database from the database using the Id number.
         * 
         * @param Id the Id of the entity in the database 
         * @return the entity corresponding to the Id
         */ 
        T GetById(int id);

        /*
         * Gets all the objects from the database of desired type.
         * 
         * @return all the desired entities in the databse
         */
        IList<T> GetAll();

        /*
         * Deletes the specified entity form the database.
         */
        void Delete(T entity);

        //IList<T> GetByFirstName(string firstName);

        //IList<T> GetByLastName(string lastName);

       // IList<T> GetByFirstAndLastName(string firstName, string lastName);

       // IList<T> GetByFullName(string firstName, char middleInitial, string lastName);

       // IList<T> GetByBirthDate(DateTime birthDate);
    }
}
