﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthCareSystem.DAL;
using HealthCareSystem.model;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    /// <summary>
    /// 
    /// </summary>
    class SQLTerminalRepository
    {
        /// <summary>
        /// The connection label
        /// </summary>
        private readonly string connectionLabel;

        /// <summary>
        /// Gets or sets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        public List<string> Columns { get; set; }
        /// <summary>
        /// Gets or sets the rows.
        /// </summary>
        /// <value>
        /// The rows.
        /// </value>
        public List<string> Rows { get; set; }
        /// <summary>
        /// Gets or sets the number of columns.
        /// </summary>
        /// <value>
        /// The number of columns.
        /// </value>
        public int NumberOfColumns { get; set; }

        /*
         * Sets connection label to the value or to a default. 
         * 
         * @param connectionLabel connection label of the desired database
         */

        /// <summary>
        /// Initializes a new instance of the <see cref="SQLTerminalRepository"/> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public SQLTerminalRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }
        /// <summary>
        /// Submits the SQL.
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <exception cref="Exception">Could not execute SQL Query</exception>
        public void SubmitSQL(string sql)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand(sql, connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            Columns = GetColumnNames(reader);
                            Rows = GetRows(reader);
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not execute SQL Query");
            }
        }

        /// <summary>
        /// Gets the column names.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        private List<string> GetColumnNames(MySqlDataReader reader)
        {
            NumberOfColumns = reader.FieldCount;
            var columnNames = new List<string>();
            for (int i = 0; i < NumberOfColumns; i++)
            {
                columnNames.Add(reader.GetName(i));
            }
            return columnNames;
        }

        /// <summary>
        /// Gets the rows.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        private List<string> GetRows(MySqlDataReader reader)
        {
            int numberOfColumns = reader.FieldCount;
            var rows = new List<string>();
            while (reader.Read())
            {
                for (int i = 0; i < numberOfColumns; i++)
                {
                    rows.Add(reader.GetValue(i).ToString());
                }
            }
        return rows;
        }
    }
}
