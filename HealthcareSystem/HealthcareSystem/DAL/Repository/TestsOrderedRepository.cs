﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.model;
using HealthCareSystem.DAL;
using HealthCareSystem.DAL.Interfaces;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    class TestsOrderedRepository : IRepository<TestOrdered>
    {

        private readonly string connectionLabel;

        /*
         * Sets connection label to the value or to a default. 
         * 
         * @param connectionLabel connection label of the desired database
         */

        public TestsOrderedRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }


        public void Add(TestOrdered ordered)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT INTO Tests_ordered VALUES (@name, @diagnosisID);",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@name", ordered.Name);
                        cmd.Parameters.AddWithValue("@diagnosisID", ordered.DiagnosisID);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception("Failed adding test result to the database.");
            }
        }

        public TestOrdered GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IList<TestOrdered> GetAll()
        {
            throw new NotImplementedException();
        }

        public IList<TestOrdered> GetAllOrderedTestsByID(int diagnosisID)
        {
            TestOrdered testOrdered = null;
            IList<TestOrdered> testsOrdered = new List<TestOrdered>();
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Test_ordered WHERE diagnosisID = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", diagnosisID);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                testOrdered = new TestOrdered()
                                {
                                    Name = reader.GetString("name"),
                                    DiagnosisID = reader.GetInt32("diagnosisID")
                                };

                                testsOrdered.Add(testOrdered);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not get the tests result to the db.");
            }
            return testsOrdered;
        }

        public void Delete(TestOrdered entity)
        {
            throw new NotImplementedException();
        }
    }
}
