﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthCareSystem.DAL.Interfaces;
using HealthCareSystem.model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL.Repository
{
    /*
     * Opens the connection to the database and stores the password info.
     */
    class PasswordsRepository : IRepository<Account>
    {
        private readonly string connectionLabel;

        /*
         * Sets connection label to the value or to a default. 
         * 
         * @param connectionLabel connection label of the desired database
         */
        public PasswordsRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }

        /*
         * Add an employee to the database.
         * 
         * @param employee the employee object that is to be added to the database
         */
        public void Add(Account account)
        {
            throw new NotImplementedException();
        }

        /*
         * Gets the desired account using the employee Id. 
         * 
         * @param Id the employee's Id number
         * @return the account with the desired employee Id
         */
        public Account GetById(int id)
        {
            {
                Account account = null;
                try
                {
                    using (var connection = DbConnection.GetConnection(connectionLabel))
                    {
                        connection.Open();
                        using (var cmd = new MySqlCommand("SELECT * FROM Passwords WHERE employeeID = @Id", connection))
                        {
                            cmd.Parameters.AddWithValue("@Id", id);
                            using (var reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    account = new Account
                                    {
                                        Id = reader.GetInt32("employeeID"),
                                        Password = reader.GetString("password")
                                    };
                                }
                            }
                        }
                        connection.Close();
                    }
                }
                catch(MySqlException exception)
                {
                    throw;
                }
                return account;
            }
        }

        /*
         * Gets all the accounts from the database.
         * 
         * @return the accounts
         */
        public IList<Account> GetAll()
        {
            IList<Account> accounts = new List<Account>();
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Passwords", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                var account = new Account
                                {
                                    Id = reader.GetInt32("employeeID"),
                                    Password = reader.GetString("password")
                                };

                                accounts.Add(account);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                Console.WriteLine("Error has occured.");
            }
            return accounts;
        }

        public void Delete(Account entity)
        {
            throw new NotImplementedException();
        }

        public IList<Account> GetByFirstName(string firstName)
        {
            throw new NotImplementedException();
        }

        public IList<Account> GetByLastName(string lastName)
        {
            throw new NotImplementedException();
        }

        public IList<Account> GetByFirstAndLastName(string firstName, string lastName)
        {
            throw new NotImplementedException();
        }

        public IList<Account> GetByFullName(string firstName, char middleInitial, string lastName)
        {
            throw new NotImplementedException();
        }

        public IList<Account> GetByBirthDate(DateTime birthDate)
        {
            throw new NotImplementedException();
        }
    }
}

