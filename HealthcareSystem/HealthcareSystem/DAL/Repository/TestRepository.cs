﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.model;
using HealthCareSystem.DAL;
using HealthCareSystem.DAL.Interfaces;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    /// <summary>
    /// Test repo connection to the db.
    /// </summary>
    /// <seealso cref="HealthCareSystem.DAL.Interfaces.IRepository{HealthcareSystem.model.Test}" />
    class TestRepository : IRepository<Test>
    {
        /// <summary>
        /// The connection label
        /// </summary>
        private readonly string connectionLabel;


        /// <summary>
        /// Initializes a new instance of the <see cref="TestRepository"/> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public TestRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }


        /// <summary>
        /// Adds the specified test.
        /// </summary>
        /// <param name="test">The test.</param>
        /// <exception cref="Exception">Could not add test.</exception>
        public void Add(Test test)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();

                    using (var cmd = new MySqlCommand("sp_AddTest", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("name",test.Name);
                        cmd.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not add test.");
            }
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Test GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the name of the by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Could not get all test.</exception>
        public Test GetByName(string name)
        {
            Test test = new Test();
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Test WHERE name = @name", connection))
                    {
                        cmd.Parameters.AddWithValue("@name", name);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                test.Name = reader.GetString(name);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not get all test.");
            }
            return test;
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception">Could not get all tests.</exception>
        public IList<Test> GetAll()
        {
            IList<Test> tests = new List<Test>();
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Test", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Test test = new Test();
                                test.Name = reader.GetString("name");
                                tests.Add(test);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not get all tests.");
            }
            return tests;
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete(Test entity)
        {
            throw new NotImplementedException();
        }
    }
}
