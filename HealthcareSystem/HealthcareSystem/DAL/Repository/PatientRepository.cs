﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthCareSystem.DAL.Interfaces;
using HealthCareSystem.model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL.Repository
{
    /*
     * The repo for the patients.
     */
    class PatientRepository : IRepository<Patient>
    {
        private readonly string connectionLabel;

        /*
         * Sets connection label to the value or to a default. 
         * 
         * @param connectionLabel connection label of the desired database
         */
        public PatientRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }

        /*
         * Add an patient to the database.
         * 
         * @param patient the patient object that is to be added to the database
         */
        public void Add(Patient patient)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT INTO Patient VALUES (null, @firstName, @midinit, @lastName, @dob, @street, @city, @state, @zipcode, @phoneNumber, @gender);",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@firstName", patient.FirstName);
                        cmd.Parameters.AddWithValue("@midinit", patient.Midinit);
                        cmd.Parameters.AddWithValue("@lastName", patient.LastName);
                        cmd.Parameters.AddWithValue("@dob", patient.DateOfBirth.Date);
                        cmd.Parameters.AddWithValue("@street", patient.Street);
                        cmd.Parameters.AddWithValue("@city", patient.City);
                        cmd.Parameters.AddWithValue("@state", patient.State);
                        cmd.Parameters.AddWithValue("@zipcode", patient.Zipcode);
                        cmd.Parameters.AddWithValue("@phoneNumber", patient.PhoneNumber);
                        cmd.Parameters.AddWithValue("@gender", patient.Gender);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception("Failed adding patient to the database.");
            }
        }


        /*
         * Gets the desired patient using the their Id. 
         * 
         * @param Id the patient's Id number
         * @return the patient with the desired Id
         */
        public Patient GetById(int id)
        {
            Patient patient = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Patient WHERE id = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                patient = new Patient
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dob"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#"),
                                    Gender = reader.GetString("gender")
                                };
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return patient;
        }

        /*
         * Gets all the patients from the database.
         * 
         * @return the patients
         */
        public IList<Patient> GetAll()
        {
            IList<Patient> patients = new List<Patient>();
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Patient", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var patient = new Patient
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dob"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#"),
                                    Gender = reader.GetString("gender")
                                };


                                patients.Add(patient);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return patients;
        }

        public void Delete(Patient patient)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "DELETE FROM Patient WHERE id = @id",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@id", patient.Id);
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception("Could not modify the database.");
            }
        }

        public IList<Patient> GetByFirstName(string firstName)
        {
            IList<Patient> patients = new List<Patient>();

            Patient patient = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Patient WHERE firstName = @firstName", connection))
                    {
                        cmd.Parameters.AddWithValue("@firstName", firstName);
                        using (var reader = cmd.ExecuteReader())
                        {
                            
                            while (reader.Read())
                            {
                                patient = new Patient
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dob"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#"),
                                    Gender = reader.GetString("gender")
                                };
                                patients.Add(patient);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return patients;
        }

        public IList<Patient> GetByLastName(string lastName)
        {
            IList<Patient> patients = new List<Patient>();

            Patient patient = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Patient WHERE lastName = @lastName", connection))
                    {
                        cmd.Parameters.AddWithValue("@lastName", lastName);
                        using (var reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                patient = new Patient
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dob"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#"),
                                    Gender = reader.GetString("gender")
                                };
                                patients.Add(patient);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return patients;
        }

        public IList<Patient> GetByFirstAndLastName(string firstName, string lastName)
        {
            IList<Patient> patients = new List<Patient>();

            Patient patient = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Patient WHERE firstName = @firstName AND lastName = @lastName", connection))
                    {
                        cmd.Parameters.AddWithValue("@firstName", firstName);
                        cmd.Parameters.AddWithValue("@lastName", lastName);
                        using (var reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                patient = new Patient
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dob"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#"),
                                    Gender = reader.GetString("gender")
                                };
                                patients.Add(patient);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return patients;
        }

        public IList<Patient> GetByFullName(string firstName, char middleInitial, string lastName)
        {
            IList<Patient> patients = new List<Patient>();

            Patient patient = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Patient WHERE firstName = @firstName AND lastName = @lastName AND midinit = @midinit", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            cmd.Parameters.AddWithValue("@firstName", firstName);
                            cmd.Parameters.AddWithValue("@midinit", middleInitial);
                            cmd.Parameters.AddWithValue("@lastName", lastName);

                            while (reader.Read())
                            {
                                patient = new Patient
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dob"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#"),
                                    Gender = reader.GetString("gender")
                                };
                                patients.Add(patient);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return patients;
        }

        public IList<Patient> GetByBirthDate(DateTime birthDate)
        {

            IList<Patient> patients = new List<Patient>();

            Patient patient = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Patient WHERE dob = @dob", connection))
                    {
                        cmd.Parameters.AddWithValue("@dob", birthDate.Date);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                patient = new Patient
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dob"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#"),
                                    Gender = reader.GetString("gender")
                                };
                                patients.Add(patient);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return patients;
        }

        public IList<Patient> GetByBirthDateAndName(DateTime birthDate, string firstName, string lastName)
        {

            IList<Patient> patients = new List<Patient>();

            Patient patient = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Patient WHERE dob = @dob AND firstName = @firstName AND lastName = @lastName", connection))
                    {
                        cmd.Parameters.AddWithValue("@dob", birthDate.Date);
                        cmd.Parameters.AddWithValue("@firstName", firstName);
                        cmd.Parameters.AddWithValue("@lastName", lastName);
                        using (var reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                patient = new Patient
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dob"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#"),
                                    Gender = reader.GetString("gender")
                                };
                                patients.Add(patient);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return patients;
        }
    }

}
