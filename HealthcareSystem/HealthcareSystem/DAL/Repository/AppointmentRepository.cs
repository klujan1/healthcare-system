﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.model;
using HealthCareSystem.DAL;
using HealthCareSystem.DAL.Interfaces;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    /// <summary>
    /// Appointment repository for the db. 
    /// </summary>
    /// <seealso cref="HealthCareSystem.DAL.Interfaces.IRepository{HealthcareSystem.model.Appointment}" />
    class AppointmentRepository : IRepository<Appointment>
    {

        /// <summary>
        /// The connection label
        /// </summary>
        private readonly string connectionLabel;

        /*
         * Sets connection label to the value or to a default. 
         * 
         * @param connectionLabel connection label of the desired database
         */

        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentRepository"/> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public AppointmentRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }

        /// <summary>
        /// Adds the specified appointment.
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        /// <exception cref="Exception">Failed adding appointment to the database.</exception>
        public void Add(Appointment appointment)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT INTO Appointment VALUES (null, @patientID, @date, @reason, @doctorID);",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@patientID", appointment.PatientID);
                        cmd.Parameters.AddWithValue("@date", appointment.Date);
                        cmd.Parameters.AddWithValue("@reason", appointment.Reason);
                        cmd.Parameters.AddWithValue("@doctorID", appointment.DoctorID);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception("Failed adding appointment to the database.");
            }
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Could not add the appointment to the db.</exception>
        public Appointment GetById(int id)
        {
            Appointment appointment = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Appointment WHERE id = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                appointment = new Appointment()
                                {
                                    Id = reader.GetInt32("id"),
                                    PatientID = reader.GetInt32("patientID"),
                                    Date = reader.GetDateTime("date"),
                                    Reason = reader.GetString("reason"),
                                    DoctorID = reader.GetInt32("doctorID")
                                };
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not add the appointment to the db.");
            }
            return appointment;
        }


        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception">Could not add the appointment to the db.</exception>
        public IList<Appointment> GetAll()
        {
            IList<Appointment> appointments = new List<Appointment>();
            Appointment appointment = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Appointment", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                appointment = new Appointment()
                                {
                                    Id = reader.GetInt32("id"),
                                    PatientID = reader.GetInt32("patientID"),
                                    Date = reader.GetDateTime("date"),
                                    Reason = reader.GetString("reason"),
                                    DoctorID = reader.GetInt32("doctorID")
                                };
                                appointments.Add(appointment);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not add the appointment to the db.");
            }
            return appointments;
        }

        /// <summary>
        /// Gets all by patient identifier.
        /// </summary>
        /// <param name="patientid">The patientid.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Could not add the appointment to the db.</exception>
        public IList<Appointment> GetAllByPatientId(int patientid)
        {
            IList<Appointment> appointments = new List<Appointment>();
            Appointment appointment = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Appointment WHERE patientID = @ID", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", patientid);
                        using (var reader = cmd.ExecuteReader())
                        {
  
                            while (reader.Read())
                            {
                                appointment = new Appointment()
                                {
                                    Id = reader.GetInt32("id"),
                                    PatientID = reader.GetInt32("patientID"),
                                    Date = reader.GetDateTime("date"),
                                    Reason = reader.GetString("reason"),
                                    DoctorID = reader.GetInt32("doctorID")
                                };
                                appointments.Add(appointment);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not add the appointment to the db.");
            }
            return appointments;
        }


        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete(Appointment entity)
        {
            throw new NotImplementedException();
        }
    }
}
