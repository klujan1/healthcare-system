﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.model;
using HealthCareSystem.DAL;
using HealthCareSystem.DAL.Interfaces;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    /// <summary>
    /// Has symptoms repository connection to the db.
    /// </summary>
    /// <seealso cref="HealthCareSystem.DAL.Interfaces.IRepository{HealthcareSystem.model.HasSymptom}" />
    class HasSymptomsRepository : IRepository<HasSymptom>
    {
        /// <summary>
        /// The connection label
        /// </summary>
        private readonly string connectionLabel;

        /*
         * Sets connection label to the value or to a default. 
         * 
         * @param connectionLabel connection label of the desired database
         */

        /// <summary>
        /// Initializes a new instance of the <see cref="HasSymptomsRepository"/> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public HasSymptomsRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }
        /// <summary>
        /// Adds the specified symptom.
        /// </summary>
        /// <param name="symptom">The symptom.</param>
        /// <exception cref="Exception">Failed adding symptom and appointment ID to the database.</exception>
        public void Add(HasSymptom symptom)
        {     
            try
            {
                    using (var connection = DbConnection.GetConnection(connectionLabel))
                    {
                        connection.Open();

                        using (var cmd = new MySqlCommand("sf_AddHasSymptom", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("appointmentID", symptom.AppointmentID);
                            cmd.Parameters.AddWithValue("symptom", symptom.SymptomName);
                            cmd.ExecuteNonQuery();
                        }
                        connection.Close();
                    }

                
            }
            catch
            {
                throw new Exception("Failed adding symptom and appointment ID to the database.");
            }
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public HasSymptom GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all symptoms by identifier.
        /// </summary>
        /// <param name="appointmentID">The appointment identifier.</param>
        /// <returns></returns>
        public IList<Symptom> GetAllSymptomsByID(int appointmentID)
        {
            IList<Symptom> symptoms = new List<Symptom>();
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT symptom FROM Has_symptoms WHERE appointmentID = @id", connection))
                    {
                        cmd.Parameters.AddWithValue("@id", appointmentID);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var symptom = new Symptom
                                {
                                    Name = reader.GetString("symptom")

                                };


                                symptoms.Add(symptom);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return symptoms;
        }
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IList<HasSymptom> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete(HasSymptom entity)
        {
            throw new NotImplementedException();
        }
    }
}
