﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.model;
using HealthCareSystem.DAL;
using HealthCareSystem.DAL.Interfaces;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    /// <summary>
    /// Diagnosis repository connection to the db. 
    /// </summary>
    /// <seealso cref="HealthCareSystem.DAL.Interfaces.IRepository{HealthcareSystem.model.Diagnosis}" />
    class DiagnosisRepository : IRepository<Diagnosis>
    {
        /// <summary>
        /// The connection label
        /// </summary>
        private readonly string connectionLabel;

        /*
         * Sets connection label to the value or to a default. 
         * 
         * @param connectionLabel connection label of the desired database
         */

        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosisRepository"/> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public DiagnosisRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }

        /// <summary>
        /// Adds the specified diagnosis.
        /// </summary>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <exception cref="Exception">Failed adding diagnosis to the database.</exception>
        public void Add(Diagnosis diagnosis)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT INTO Diagnosis VALUES (null, @appointmentID, @init, @final);",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@appointmentID", diagnosis.AppointmentID);
                        cmd.Parameters.AddWithValue("@init", diagnosis.InitialDiagnosis);
                        cmd.Parameters.AddWithValue("@final", diagnosis.FinalDiagnosis);

                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception("Failed adding diagnosis to the database.");
            }
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Could not add the appointment to the db.</exception>
        public Diagnosis GetById(int id)
        {
            Diagnosis diagnosis = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Diagnosis WHERE id = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                diagnosis = new Diagnosis()
                                {
                                    Id = reader.GetInt32("id"),
                                    AppointmentID = reader.GetInt32("appointmentID"),
                                    InitialDiagnosis = reader.GetString("intialDiagnosis"),
                                    FinalDiagnosis = reader.GetString("finalString")
                                };
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not add the appointment to the db.");
            }
            return diagnosis;
        }

        /// <summary>
        /// Gets all diagnosis by appointment identifier.
        /// </summary>
        /// <param name="appointmentID">The appointment identifier.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Could not add the test result to the db.</exception>
        public Diagnosis GetAllDiagnosisByAppointmentID(int appointmentID)
        {
            Diagnosis result = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Diagnosis WHERE appointmentID = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", appointmentID);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                try
                                {
                                    result = new Diagnosis()
                                    {
                                        Id = reader.GetInt32("id"),
                                        AppointmentID = reader.GetInt32("appointmentID"),
                                        InitialDiagnosis = reader.GetString("initialDiagnosis"),
                                        FinalDiagnosis = reader.GetString("finalDiagnosis")
                                    };
                                }
                                catch
                                {
                                    result = new Diagnosis()
                                    {
                                        Id = reader.GetInt32("id"),
                                        AppointmentID = reader.GetInt32("appointmentID"),
                                        InitialDiagnosis = reader.GetString("initialDiagnosis"),
                                        FinalDiagnosis = null
                                    };
                                }

                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not add the test result to the db.");
            }
            return result;
        }

        /// <summary>
        /// Finalizes the diagnosis.
        /// </summary>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <exception cref="Exception">Failed adding final diagnosis to the database.</exception>
        public void FinalizeDiagnosis(Diagnosis diagnosis)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "UPDATE Diagnosis SET finalDiagnosis = @finalDiagnosis WHERE appointmentID = @appointmentID;",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@finalDiagnosis", diagnosis.FinalDiagnosis);
                        cmd.Parameters.AddWithValue("@appointmentID", diagnosis.AppointmentID);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception("Failed adding final diagnosis to the database.");
            }
        }

        /// <summary>
        /// Determines whether [is final diagnosis null] [the specified identifier].
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///   <c>true</c> if [is final diagnosis null] [the specified identifier]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="Exception">Could not add the test result to the db.</exception>
        public bool isFinalDiagnosisNull(int id)
        {
            Diagnosis result = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT finalDiagnosis FROM Diagnosis WHERE appointmentID = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            reader.Read();
                            string finalDiagnosis = reader.GetString("finalDiagnosis");
                            if (finalDiagnosis.Length == 0 || finalDiagnosis == null)
                            {
                                return false;
                            }
                            return true;
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return true;
            }
        }

        public bool isInitialDiagnosisNull(int id)
        {
            Diagnosis result = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT initialDiagnosis FROM Diagnosis WHERE appointmentID = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            return !reader.Read();
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IList<Diagnosis> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete(Diagnosis entity)
        {
            throw new NotImplementedException();
        }
    }
}
