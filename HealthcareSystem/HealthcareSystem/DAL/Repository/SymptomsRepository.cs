﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.model;
using HealthCareSystem.DAL;
using HealthCareSystem.DAL.Interfaces;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository

{
    /// <summary>
    /// Symptoms repo conneciton to the db.
    /// </summary>
    /// <seealso cref="HealthCareSystem.DAL.Interfaces.IRepository{HealthcareSystem.model.Symptom}" />
    public class SymptomsRepository : IRepository<Symptom>
    {
        /// <summary>
        /// The connection label
        /// </summary>
        private readonly string connectionLabel;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymptomsRepository"/> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public SymptomsRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;

        }
        /// <summary>
        /// Adds the specified symptom.
        /// </summary>
        /// <param name="symptom">The symptom.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Add(Symptom symptom)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Symptom GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public IList<Symptom> GetAll()
        {
            IList<Symptom> symptoms = new List<Symptom>();
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Symptoms", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var symptom  = new Symptom
                                {
                                    Name = reader.GetString("symptom")
                                   
                                };


                                symptoms.Add(symptom);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return symptoms;
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete(Symptom entity)
        {
            throw new NotImplementedException();
        }
    }
}