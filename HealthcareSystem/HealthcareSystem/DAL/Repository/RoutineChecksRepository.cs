﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.model;
using HealthCareSystem.DAL;
using HealthCareSystem.DAL.Interfaces;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    /// <summary>
    /// Routine checks repository connection to db.
    /// </summary>
    /// <seealso cref="HealthCareSystem.DAL.Interfaces.IRepository{HealthcareSystem.model.RoutineCheck}" />
    class RoutineChecksRepository : IRepository<RoutineCheck>
    {
        /// <summary>
        /// The connection label
        /// </summary>
        private readonly string connectionLabel;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoutineChecksRepository"/> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public RoutineChecksRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }

        /// <summary>
        /// Adds the specified routine check.
        /// </summary>
        /// <param name="routineCheck">The routine check.</param>
        /// <exception cref="Exception">Failed adding routine checks to the database.</exception>
        public void Add(RoutineCheck routineCheck)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT INTO Routine_checks VALUES (@appointmentID, @nurseID, @bloodPressure, @bodyTemp, @pulse, @systolicReading, @diastolicReading);",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@appointmentID", routineCheck.AppointmentID);
                        cmd.Parameters.AddWithValue("@nurseID", routineCheck.NurseID);
                        cmd.Parameters.AddWithValue("@bloodPressure", routineCheck.BloodPressure);
                        cmd.Parameters.AddWithValue("@bodyTemp", routineCheck.BodyTemp);
                        cmd.Parameters.AddWithValue("@pulse", routineCheck.PulseBPM);
                        cmd.Parameters.AddWithValue("@systolicReading", routineCheck.SystolicReading);
                        cmd.Parameters.AddWithValue("@diastolicReading", routineCheck.DiastolicReading);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception("Failed adding routine checks to the database.");
            }
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Could get the checks.</exception>
        public RoutineCheck GetById(int id)
        {
            RoutineCheck check = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Routine_checks WHERE appointmentID = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                check = new RoutineCheck()
                                {
                                    AppointmentID = reader.GetInt32("appointmentID"),
                                    BloodPressure = reader.GetString("bloodPressure"),
                                    BodyTemp = reader.GetInt32("bodyTemp"),
                                    PulseBPM = reader.GetInt32("pulseBPM"),
                                    SystolicReading = reader.GetInt32("systolicReading"),
                                    DiastolicReading = reader.GetInt32("diastolicReading")
                                    
                                };
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could get the checks.");
            }
            return check;
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IList<RoutineCheck> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete(RoutineCheck entity)
        {
            throw new NotImplementedException();
        }
    }
}
