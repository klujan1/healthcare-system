﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareSystem.model;
using HealthCareSystem.DAL.Interfaces;
using HealthCareSystem.DAL;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    /// <summary>
    /// Test results connection 
    /// </summary>
    /// <seealso cref="HealthCareSystem.DAL.Interfaces.IRepository{HealthcareSystem.model.TestResult}" />
    class TestResultsRepository : IRepository<TestResult>
    {
        /// <summary>
        /// The connection label
        /// </summary>
        private readonly string connectionLabel;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestResultsRepository"/> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public TestResultsRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }

        /// <summary>
        /// Adds the specified result.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <exception cref="Exception">Failed adding test result to the database.</exception>
        public void Add(TestResult result)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT INTO Test_results VALUES (null, @name, @diagnosisID, @result, @datePerformed);",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@name", result.TestName);
                        cmd.Parameters.AddWithValue("@diagnosisID", result.DiagnosisID);
                        cmd.Parameters.AddWithValue("@result", result.Result);
                        cmd.Parameters.AddWithValue("@datePerformed", result.DatePerformed);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception("Failed adding test result to the database.");
            }
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Could not add the test result to the db.</exception>
        public TestResult GetById(int id)
        {
            TestResult result = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Test_results WHERE id = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                result = new TestResult()
                                {
                                    Id = reader.GetInt32("id"),
                                    TestName = reader.GetString("testName"),
                                    DiagnosisID = reader.GetInt32("diagnosisID"),
                                    Result = reader.GetString("result"),
                                    DatePerformed = reader.GetDateTime("datePerformed")
                                };
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not add the test result to the db.");
            }
            return result;
        
    
}

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception">Could not add the test result to the db.</exception>
        public IList<TestResult> GetAll()
        {
            IList<TestResult> results = new List<TestResult>();
            TestResult result = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Test_results", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result = new TestResult()
                                {
                                    Id = reader.GetInt32("id"),
                                    TestName = reader.GetString("testName"),
                                    DiagnosisID = reader.GetInt32("diagnosisID"),
                                    Result = reader.GetString("result"),
                                    DatePerformed = reader.GetDateTime("datePerformed")
                                };
                                results.Add(result);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not add the test result to the db.");
            }
            return results;
        }

        /// <summary>
        /// Gets all by patient identifier.
        /// </summary>
        /// <param name="diagnosisID">The diagnosis identifier.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Could not add the test result to the db.</exception>
        public IList<TestResult> GetAllByPatientId(int diagnosisID)
        {
            IList<TestResult> results = new List<TestResult>();
            TestResult result = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Test_results WHERE diagnosisID = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", diagnosisID);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result = new TestResult()
                                {
                                    Id = reader.GetInt32("id"),
                                    TestName = reader.GetString("testName"),
                                    DiagnosisID = reader.GetInt32("diagnosisID"),
                                    Result = reader.GetString("result"),
                                    DatePerformed = reader.GetDateTime("datePerformed")
                                };
                                results.Add(result);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Could not add the test result to the db.");
            }
            return results;
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete(TestResult entity)
        {
            throw new NotImplementedException();
        }
    }
}
