﻿using System;
using System.Collections.Generic;
using System.Data;
using HealthCareSystem.DAL.Interfaces;
using HealthCareSystem.model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL.Repository
{
    /// <summary>
    /// Employee Repository connection to the db.
    /// </summary>
    /// <seealso cref="HealthCareSystem.DAL.Interfaces.IRepository{HealthCareSystem.model.Employee}" />
    internal class EmployeeRepository : IRepository<Employee>
    {
        /// <summary>
        /// The connection label
        /// </summary>
        private readonly string connectionLabel;

        /*
         * Sets connection label to the value or to a default. 
         * 
         * @param connectionLabel connection label of the desired database
         */

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeRepository"/> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public EmployeeRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connectionLabel = connectionLabel;
        }

        /*
         * Add an employee to the database.
         * 
         * @param employee the employee object that is to be added to the database
         */

        /// <summary>
        /// Adds the specified employee.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <exception cref="Exception"></exception>
        public void Add(Employee employee)
        {
            int id = 0;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT INTO Employee VALUES (null, @FirstName, @midinit, @lastName, @dob, @street, @city, @state, @zipcode, @phoneNumber);",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@FirstName", employee.FirstName);
                        cmd.Parameters.AddWithValue("@midinit", employee.Midinit);
                        cmd.Parameters.AddWithValue("@lastName", employee.LastName);
                        cmd.Parameters.AddWithValue("@dob", employee.DateOfBirth);
                        cmd.Parameters.AddWithValue("@street", employee.Street);
                        cmd.Parameters.AddWithValue("@city", employee.City);
                        cmd.Parameters.AddWithValue("@state", employee.State);
                        cmd.Parameters.AddWithValue("@zipcode", employee.Zipcode);
                        cmd.Parameters.AddWithValue("@phoneNumber", employee.PhoneNumber);
                        cmd.ExecuteNonQuery();
                    }
                    
                    using (var cmd = new MySqlCommand("SELECT id FROM Employee ORDER BY id DESC LIMIT 0, 1", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                id = reader.GetInt32("id");
                            }
                        }
                    }

                    using (var cmd = new MySqlCommand("INSERT INTO Nurse VALUES (@Id);", connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception();
            }
        }

        /*
         * Gets the employee using their Id.
         * 
         * @param Id the Id of the employee
         * @return the employee with the desired Id
         */
        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Employee GetById(int id)
        {
            Employee employee = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Employee WHERE id = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                employee = new Employee
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dateOfBirth"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#")
                                };
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return employee;
        }

        /*
         * Gets all the employees in the database.
         * 
         * @return the employees
         */

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public IList<Employee> GetAll()
        {
            IList<Employee> employees = new List<Employee>();
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Employee", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var employee = new Employee
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dateOfBirth"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#")
                                };


                                employees.Add(employee);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return null;
            }
            return employees;
        }

        /// <summary>
        /// Deletes the specified employee.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <exception cref="Exception">Could not modify the database.</exception>
        public void Delete(Employee employee)
        {
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd =
                            new MySqlCommand(
                                "DELETE FROM Employee WHERE id = @id",
                                connection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@id", employee.Id);
                        connection.Close();
                    }
                }
            }
            catch
            {
                throw new Exception("Could not modify the database.");
            }
        }


        /// <summary>
        /// Determines whether the specified identifier is admin.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///   <c>true</c> if the specified identifier is admin; otherwise, <c>false</c>.
        /// </returns>
        public bool isAdmin(int id)
        {
            Employee employee = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Admin WHERE id = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                return true;
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        /// <summary>
        /// Determines whether the specified identifier is doctor.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///   <c>true</c> if the specified identifier is doctor; otherwise, <c>false</c>.
        /// </returns>
        public bool isDoctor(int id)
        {
            Employee employee = null;
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand("SELECT * FROM Doctor WHERE id = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                return true;
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        /// <summary>
        /// Gets all doctors.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception">Doctor information could not be retrieved.</exception>
        public IList<Employee> GetAllDoctors()
        {
            IList<Employee> doctors = new List<Employee>();
            try
            {
                using (var connection = DbConnection.GetConnection(connectionLabel))
                {
                    connection.Open();
                    using (
                        var cmd = new MySqlCommand("SELECT * FROM Employee e, Doctor d WHERE e.id = d.id", connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var employee = new Employee
                                {
                                    Id = reader.GetInt32("id"),
                                    FirstName = reader.GetString("firstName"),
                                    Midinit = reader.GetString("midinit"),
                                    LastName = reader.GetString("lastName"),
                                    DateOfBirth = reader.GetDateTime("dateOfBirth"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zipcode = reader.GetString("zipcode"),
                                    PhoneNumber = reader.GetString("phone#")
                                };


                                doctors.Add(employee);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Doctor information could not be retrieved.");
            }
            return doctors;
        }
    }
}