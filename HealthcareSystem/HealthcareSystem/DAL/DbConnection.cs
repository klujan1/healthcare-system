﻿using System.Configuration;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    /*
     * DB connection.
     */
    internal class DbConnection
    {
        /*
         * Gets the connection of the database.
         * 
         * @param connectionLabel the connection label for the database
         * @return the sql connection
         */
        public static MySqlConnection GetConnection(string connectionLabel)
        {
            string connection = ConfigurationManager.ConnectionStrings[connectionLabel].ConnectionString;
            return new MySqlConnection(connection);
        }
    }
}